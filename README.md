
# Demo HFT Market Maker in Python

Simple demo app that performs a common market making algorithm on the Kucoin Crypto Exchange.

## Setup

You will need to create an .env file (just named .env, nothing else) in the root directory of the project and populate it with the following values:

```python
KUCOIN_API_SECRET='your-secret'
KUCOIN_API_KEY='your-key'
KUCOIN_API_PASSWORD='your-api-password'
```
Note: This password is the one created for the API key and not the 6-digit 'trading password'

Other configuration parameters are as follows:

```python
A_PARAM=0.9
K_PARAM=0.3
GAMMA_PARAM=1.0
MU_PARAM=0.0
SIGMA_PARAM=0.03
MIN_Q_PARAM=-0.0004
MAX_Q_PARAM=0.0004
REST_PERIOD=0.066
TEST_MODE=False
SYMBOL=ETH-USDT
```
The parameters are discussed below in the algorithm discussion section.

MIN_Q_PARAM and MAX_Q_PARAM are the position limits long and short

REST_PERIOD is the amount of time in seconds between order quote updates. The server API max is 
15 times / second, so that equates to a period of 0.066s

TEST_MODE controls whether orders are sent. If it is False then real trades will be sent to the server.

SYMBOL controls which ticker is being traded. Accounts must exist for the currencies on the Kucoin server first

I am running locally using python 3.9 and poetry.

Run it with the standard command for poetry:

```commandline
poetry run main.py
```

Demo output:

![A screenshot of the GUI during operation](media/gui-demo.png)


## The Market Making Algorithm

The algorithm implemented here is a variation of the Avellaneda–Stoikov algorithm. This algorithm was originally 
published in 2008 in a paper called: "High-frequency trading in a limit order book" in the Journal of Quantitative 
Finance by Avellaneda and Stoikov.

The original version assumes that the market will close at a certain time each day (like 4pm for US Equities), and 
adjusts the quotes so that the market maker will hold no position and take no risk overnight while the market is not 
trading.

The version implemented here is the adjusted formula that assumes the market does not close which is more appropriate 
for a market making algorithm for a cryptocurrency exchange. This version also assumes that the underlying price 
process is capable of drift.  

This version was originally published in Olivier Gueant's 2012 paper, "Dealing with the inventory risk: a solution to 
the market making problem". Professor Gueant's paper assumes the market maker sets a risk limit of Q shares (or -Q if 
they are short). This assumption on bounds allows for the solution of Bellman equations from Stochastic Optimal Control 
that allowed him to produce a formula that solves the differential equations.

### The Equations

![Formulas for calculating the Bid-Ask spread](media/gueant-eqns.png)

Where delta_b is the bid price difference from the midpoint price, delta_a is the ask price difference, and phi is 
the absolute distance of the bid-ask spread.

Gamma is the Risk Factor in the model and ranges from 0 (risky) to 1 (lower risk) and is used to control how 
symmetrical quotes are around the midpoint price. For example, if many buy orders come and the market maker ends up 
with a short position, the spread will be adjusted higher so the market maker is less likely to sell and more likely 
to buy and return them to a neutral position.

The k and A parameters control the model's assumptions about the liquidity of the order book and arrival rates of 
trades at different price levels. The arrival rates are modeled as Poisson processes and the closer a bid or ask is 
to the midpoint price, the faster trades will arrive. An exponential distribution is used as a parametric fit:

![Order book liquidity model](media/gueant-arrival-rates.png)

where k is the exponent modeling how quickly orders drop as the price gets farther from the midpoint and A is the 
scaling factor that scales the distribution to different rates of orders / second. For example, a sleepy market could 
have orders drop at the same rate (same k) as an active market at the same distance from the midpoint, but A models 
sleepy or active.

The parameters mu and sigma model the price process, which in Professor Gueant's paper is modeled as an arithmetic 
Brownian motion.

Finally, the q parameter represents the current position of the market maker in units or shares of the traded asset.

### Configuration

These parameters are also set through the .env file:

```python
A_PARAM=0.9
K_PARAM=0.3
GAMMA_PARAM=1.0
MU_PARAM=0.0
SIGMA_PARAM=0.03
```

these are the demo parameters in Professor Gueant's paper, however they need to be fit to each asset and 
each order book since the model uses a parametric approach
