"""
Module for initializing the program threads and for shutting down the system
"""
import logging
import asyncio
import threading

from gui.renderer import OrderBookRenderer
from strategy import StrategyRunner
from state_manager import StateManager
from kucoin.rest.client import KucoinRestClient
from kucoin.rest.rest_response_parsing import (
    wait_and_parse_websocket_channel_info, wait_and_parse_order_book_snapshot, wait_and_parse_get_symbol_info,
    wait_and_parse_get_ticker, wait_and_parse_get_accounts, wait_and_parse_get_isolated_margin_account_info
)
from kucoin.websocket.websocket import KuCoinWebsocket
from kucoin.websocket.ws_utils import message_processing_coroutine


# Create a logger object
logger = logging.getLogger('system')

# Set the log level to DEBUG so that all messages are logged
logger.setLevel(logging.DEBUG)


def system_shutdown_callback(renderer: OrderBookRenderer):
    """
    Callback that shuts down the system on some event like the user pressing the X to close the GUI

    :param renderer: The renderer displaying the GUI
    """
    logger.info('Initiating System Shutdown')

    rest_client = renderer.state.rest_client

    # Gracefully terminate the websocket, rest session, strategy, and renderer
    renderer.state.websocket_keep_running = False
    renderer.state.strategy_keep_running = False
    rest_client.terminate()
    renderer.terminate()

    logger.info('Shutdown Callback Completed')


async def make_initialization_rest_requests(rest_client: KucoinRestClient, symbol: str):
    """
    Requests the state information from the REST API needed to set up the system like account balances,
    trading ticker information, websocket config, etc. and initializes the StateManager

    :param rest_client: The KucoinRestClient used to connect to the API
    :param symbol: The trading symbol ticker like BTC-USDT
    :return: The StateManager for the system
    """
    response_future = await rest_client.request_websocket_channel_info()
    ws_info = await wait_and_parse_websocket_channel_info(response_future)

    # get the symbol info like price increment, etc.
    response_future = await rest_client.get_all_symbol_info()
    symbol_info = await wait_and_parse_get_symbol_info(response_future, symbol)

    # get the ticker info like current best bid and ask
    response_future = await rest_client.get_ticker(symbol)
    ticker_info = await wait_and_parse_get_ticker(response_future)

    # get the account balances
    response_future = await rest_client.get_accounts()

    currencies = symbol_info.symbol.split('-')
    base_symbol = currencies[0]
    quote_symbol = currencies[1]

    accounts = await wait_and_parse_get_accounts(response_future, acct_type='isolated',
                                                 currencies=[base_symbol, quote_symbol])

    # get the account margin info for short positions
    response_future = await rest_client.get_isolated_margin_account_info(symbol)
    margin_info = await wait_and_parse_get_isolated_margin_account_info(response_future)

    base_currency_account = accounts[base_symbol]
    quote_currency_account = accounts[quote_symbol]

    logger.info(ticker_info)
    logger.info(symbol_info)

    state = StateManager(
        symbol_info=symbol_info,
        ticker_info=ticker_info,
        websocket_info=ws_info,
        base_currency_account=base_currency_account,
        quote_currency_account=quote_currency_account,
        margin_info=margin_info,
        rest_client=rest_client
    )

    return state


async def initialize_server_system(loop, renderer, strategy, symbol):
    """
    Initializes the system thread for communication with the server. Configures the REST API, the
    websocket, and the renderer

    :param loop: The asyncio event loop to schedule coroutines
    :param renderer: The renderer that displays the GUI to the user
    :param strategy: The strategy that will execute using the API connections
    :param symbol: The ticker symbol we are interested in
    """
    # set the renderer to call the shutdown callback when the X is pressed on the GUI
    renderer.set_shutdown_callback(system_shutdown_callback)

    # start the REST API and get the information needed to configure the websocket and state
    rest_client = KucoinRestClient(loop)

    state = await make_initialization_rest_requests(rest_client, symbol)

    websocket = KuCoinWebsocket(async_evt_loop=loop, state=state, private_ws=True)

    # request the subscriptions we are interested in on the websocket
    await websocket.subscribe(f'/market/level2:{symbol}', private_msg=False)
    await websocket.subscribe(f'/market/match:{symbol}', private_msg=False)
    await websocket.subscribe(f'/account/balance', private_msg=True)

    # give the server a moment to start sending the websocket messages so we have the updates in the
    # buffer when we request the order book snapshot
    await asyncio.sleep(2)

    # get the snapshot of the order book
    order_book_response_future = await rest_client.get_order_book_snapshot(symbol)
    state.order_book = await wait_and_parse_order_book_snapshot(
        order_book_response_future, state.ticker_info.price, state.symbol_info.price_increment)

    renderer.set_state(state)
    strategy.set_state(state)

    tasks = [
        asyncio.create_task(message_processing_coroutine(websocket, state))
    ]

    # will block the length of the program while the websocket runs
    await asyncio.gather(*tasks)

    # wait during shutdown
    while not websocket.closed:
        await asyncio.sleep(0.01)

    logger.info('Server coroutine processing complete')


async def initialize_strategy_system(strategy):
    """
    Initializes the system thread for running the strategy logic and requesting orders

    :param strategy: The trading strategy to run
    """
    while strategy.create_order_rate_limiter is None:
        await asyncio.sleep(0.001)

    tasks = [
        asyncio.create_task(strategy.loop()),
        asyncio.create_task(strategy.create_order_rate_limiter.handle_requests())
    ]

    await asyncio.gather(*tasks)

    logger.info('Strategy coroutine processing complete')


def start_server_async_coroutine_loop(renderer, strategy, symbol):
    """
    Starts the event loop for scheduling coroutines in this thread and then initializes the system

    :param renderer: The renderer managing the GUI
    :param strategy: The trading strategy to run
    :param symbol: The symbol we are interested in
    """
    # create a new event loop in this thread for scheduling coroutines
    loop = asyncio.new_event_loop()
    loop.run_until_complete(initialize_server_system(loop, renderer, strategy, symbol))


def start_strategy_async_coroutine_loop(strategy):
    """
    Starts the event loop for scheduling coroutines in this thread and then initializes the strategy

    :param strategy: The trading strategy to run
    """
    loop = asyncio.new_event_loop()
    loop.run_until_complete(initialize_strategy_system(strategy))


def run_server_coroutine_thread(renderer, strategy, symbol):
    """
    Starts the thread that runs async coroutines so that they do not interfere with the GUI rendering event loop
    which is required to be located in the main thread

    :param renderer: The renderer managing the GUI
    :param strategy: The trading strategy to run
    :param symbol: The symbol we are interested in
    """
    _t = threading.Thread(target=start_server_async_coroutine_loop, args=(renderer, strategy, symbol))
    _t.start()
    return _t


def run_strategy_coroutine_thread(strategy):
    """
    Starts the thread that runs async coroutines for the trading strategy

    :param strategy: The trading strategy to run
    """
    _t = threading.Thread(target=start_strategy_async_coroutine_loop, args=(strategy,))
    _t.start()
    return _t


def main_fcn(symbol: str):
    """
    The main function of the application. Launches 3 threads that are used for the program. One for running server
    interactions like parsing websocket messages, one for running the trading strategy logic, and the main one
    is required by the tkinter gui framework to process events like clicks

    :param symbol: The symbol we are interested in, e.g. BTC-USDT
    """
    renderer = OrderBookRenderer()
    strat = StrategyRunner()

    # launch the threads for processing coroutines
    _serv_t = run_server_coroutine_thread(renderer, strat, symbol)
    _strat_t = run_strategy_coroutine_thread(strat)

    # run the GUI main event loop for processing user interactions as required by tkinter
    renderer.loop()

    logger.info('Main Thread Shutdown')
