
import pandas as pd

import tkinter
from tkinter import Tk
from tkinter.ttk import LabelFrame, Treeview
from matplotlib.figure import Figure
from matplotlib.backends.backend_tkagg import (FigureCanvasTkAgg, NavigationToolbar2Tk)
import seaborn as sns

from decimal import Decimal

from typing import Tuple

from order_book import BookSide, OrderBook
from trade_fills_log import FILL_COLS, TradeFills
from kucoin.kucoin_dataclasses import AccountInfo


def launch_gui() -> Tuple[Tk, Treeview, Treeview, Treeview, Treeview, Figure, FigureCanvasTkAgg]:
    """
    Creates the GUI components we will use to display info to the user

    :return: The GUI components for the renderer
    """
    window = Tk()
    window.title('Demo HFT Market Maker in Python')
    window.geometry('1500x1000')

    # define out bids, asks, and fills
    bid_panel = LabelFrame(window, text='Bids')
    ask_panel = LabelFrame(window, text='Asks')
    fill_panel = LabelFrame(window, text='Fills')
    account_panel = LabelFrame(window, text='Accounts', height=50)
    _plot_panel = LabelFrame(window, text='Order Depth Plot')

    # specify the rows and columns
    window.rowconfigure(0, weight=1)
    window.rowconfigure(1, weight=1)
    window.rowconfigure(2, weight=6)
    window.rowconfigure(3, weight=1)
    window.rowconfigure(4, weight=1)
    window.columnconfigure(0, weight=1)
    window.columnconfigure(1, weight=1)

    # create a panel for displaying current bids by price and qty
    bid_tv = Treeview(bid_panel, columns=('Price', 'Quantity'), show='headings')
    bid_tv.heading('#1', text='Price')
    bid_tv.heading('#2', text='Quantity')
    bid_tv.pack(fill='both')
    bid_panel.grid(row=0, column=0, sticky='nsew')

    # create a panel for displaying current asks by price and qty
    ask_tv = Treeview(ask_panel, columns=('Price', 'Quantity'), show='headings')
    ask_tv.heading('#1', text='Price')
    ask_tv.heading('#2', text='Quantity')
    ask_tv.pack(fill='both')
    ask_panel.grid(row=0, column=1, sticky='nsew')

    fill_tv = Treeview(fill_panel, columns=(
        FILL_COLS[0], FILL_COLS[1], FILL_COLS[2],
        FILL_COLS[3], FILL_COLS[4], FILL_COLS[5],
        FILL_COLS[6], FILL_COLS[7]), show='headings')

    # set the column names
    fill_tv.heading("#1", text=FILL_COLS[0])
    fill_tv.heading("#2", text=FILL_COLS[1])
    fill_tv.heading("#3", text=FILL_COLS[2])
    fill_tv.heading("#4", text=FILL_COLS[3])
    fill_tv.heading("#5", text=FILL_COLS[4])
    fill_tv.heading("#6", text=FILL_COLS[5])
    fill_tv.heading("#7", text=FILL_COLS[6])
    fill_tv.heading("#8", text=FILL_COLS[7])

    # set the column sizes
    fill_tv.column("#1", width=132)
    fill_tv.column("#2", width=132)
    fill_tv.column("#3", width=132)
    fill_tv.column("#4", width=132)
    fill_tv.column("#5", width=132)
    fill_tv.column("#6", width=132)
    fill_tv.column("#7", width=132)
    fill_tv.column("#8", width=132)

    fill_tv.pack(fill='both')
    fill_panel.grid(row=1, column=0, columnspan=2, sticky='nsew')

    account_tv = Treeview(account_panel, columns=(
        'Quote Currency', 'Total Balance', 'Available Balance', 'Hold Balance',
        'Base Currency', 'Total Balance', 'Available Balance', 'Hold Balance', 'Current Inventory'), show='headings')
    account_tv.heading('#1', text='Quote Currency')
    account_tv.heading('#2', text='Total Balance')
    account_tv.heading('#3', text='Available Balance')
    account_tv.heading('#4', text='Hold Balance')
    account_tv.heading('#5', text='Base Currency')
    account_tv.heading('#6', text='Total Balance')
    account_tv.heading('#7', text='Available Balance')
    account_tv.heading('#8', text='Hold Balance')
    account_tv.heading('#9', text='Current Inventory')

    # set the column sizes
    account_tv.column("#1", width=160)
    account_tv.column("#2", width=160)
    account_tv.column("#3", width=160)
    account_tv.column("#4", width=160)
    account_tv.column("#5", width=160)
    account_tv.column("#6", width=160)
    account_tv.column("#7", width=160)
    account_tv.column("#8", width=160)
    account_tv.column("#9", width=160)

    account_tv.pack(fill='both', expand=True)
    account_panel.grid(row=2, column=0, columnspan=2, sticky='nsew')
    # account_panel.grid(row=2, column=0, columnspan=2)

    # the figure that will contain the order book plot
    fig = Figure(figsize=(5, 5), dpi=100)

    # adding the subplot plotting the graph
    plot = fig.add_subplot(111)
    plot.plot([])

    # creating the Tkinter canvas
    # containing the Matplotlib figure
    fig_canvas = FigureCanvasTkAgg(fig, master=window)
    fig_canvas.draw()

    # placing the canvas on the Tkinter window
    fig_canvas.get_tk_widget().grid(row=3, column=0, columnspan=2, sticky='nsew')

    # create the panel for holding the plot
    toolbar_panel = LabelFrame(window)
    toolbar_panel.grid(row=4, column=0, columnspan=2, sticky='nsew')

    # creating the Matplotlib toolbar to zoom and control the plot
    toolbar = NavigationToolbar2Tk(fig_canvas, toolbar_panel)
    toolbar.pack(side=tkinter.TOP, fill=tkinter.X)

    return window, bid_tv, ask_tv, fill_tv, account_tv, fig, fig_canvas


def draw_bids(bid_book: BookSide, bid_tv: Treeview):
    """
    Renders the bid order book data structure in the frontend Treeview

    :param bid_book: The bid book data structure
    :param bid_tv: The Treeview to render
    """
    bid_tv.delete(*bid_tv.get_children())

    if len(bid_book.amounts) == 0:
        return

    best_bid_i = bid_book.get_best_index()
    for i in range(best_bid_i, best_bid_i + 1000):

        price = bid_book.prices[i]
        amount = bid_book.amounts[i]
        if amount == Decimal('0'):
            continue

        bid_tv.insert(parent='', index=i, iid=str(i), text='', values=(price, amount))
        length = len(bid_tv.get_children())

        if length > 10:
            break


def draw_asks(ask_book: BookSide, ask_tv: Treeview):
    """
    Renders the ask order book data structure in the frontend Treeview

    :param ask_book: The ask book data structure
    :param ask_tv: The Treeview to render
    """
    ask_tv.delete(*ask_tv.get_children())

    if len(ask_book.amounts) == 0:
        return

    best_ask_i = ask_book.get_best_index()
    for i in range(best_ask_i, best_ask_i + 1000):

        price = ask_book.prices[i]
        amount = ask_book.amounts[i]
        if amount == Decimal('0'):
            continue

        ask_tv.insert(parent='', index=i, iid=str(i), text='', values=(price, amount))
        length = len(ask_tv.get_children())

        if length > 10:
            break


def draw_fills(fill_log: TradeFills, fill_tv: Treeview, rows=15):
    """
    Renders the fill history data structure in the frontend Treeview

    :param fill_log: The data structure containing the log of filled trades
    :param fill_tv: The Treeview to render
    :param rows: The number of past trade rows to render in the table
    """
    fill_tv.delete(*fill_tv.get_children())

    fill_index = fill_log.fill_id_num - rows

    if fill_index < 0:
        fill_index = fill_log.max_fills + fill_index

    for i, j in zip(range(fill_index, fill_index + rows), range(0, rows)):
        i = i % fill_log.max_fills
        fill = fill_log.fills[i]

        fill_tv.insert(parent='', index=j, iid=str(j), text='', values=(
                fill.get(FILL_COLS[0]), fill.get(FILL_COLS[1]),
                fill.get(FILL_COLS[2]), fill.get(FILL_COLS[3]), fill.get(FILL_COLS[4]),
                fill.get(FILL_COLS[5]), fill.get(FILL_COLS[6], fill.get(FILL_COLS[7]))
            ))


def draw_order_book_depth(order_book: OrderBook, fig: Figure, fig_canvas: FigureCanvasTkAgg):
    """
    Draws the plot of the order book depth based on the current state of the OrderBook

    :param order_book: The data to plot
    :param fig: The matplotlib fig to plot on
    :param fig_canvas: The canvas object to render the plot
    :return: void
    """
    # clear the old plot data
    fig.clf()

    # Set the title
    ax = fig.gca()
    ax.set_title('Order Book Depth')

    # create the ECDF plots for the order book
    # wrap the data structures in DataFrames for seaborn
    best_bid_i = order_book.bids.get_best_index()
    best_ask_i = order_book.asks.get_best_index()

    bid_prices = order_book.bids.prices[best_bid_i:best_bid_i+200]
    ask_prices = order_book.asks.prices[best_ask_i:best_ask_i+200]

    bid_qtys = order_book.bids.amounts[best_bid_i:best_bid_i+200]
    ask_qtys = order_book.asks.amounts[best_ask_i:best_ask_i+200]

    bid_df = pd.DataFrame({'price': bid_prices, 'quantity': bid_qtys})
    ask_df = pd.DataFrame({'price': ask_prices, 'quantity': ask_qtys})

    # plot bid side
    sns.ecdfplot(x='price', weights='quantity', stat='count', complementary=True, data=bid_df, ax=ax, color='g')

    # plot ask side
    sns.ecdfplot(x='price', weights='quantity', stat='count', data=ask_df, ax=ax, color='r')

    # Set the plot labels
    ax.set_xlabel('Price')
    ax.set_ylabel('Quantity')

    # draw the new plot
    fig_canvas.draw()


def draw_current_quote(current_bid, current_ask, fig, fig_canvas):
    """
    Draws the current bid and ask quotes selected by the strategy

    :param current_bid: The current bid
    :param current_ask: The current ask
    :param fig: The matplotlib figure
    :param fig_canvas: The canvas the plot is being drawn on
    """
    lines = []
    colors = []
    if current_bid is not None:
        lines.append(current_bid)
        colors.append('tab:orange')
    if current_ask is not None:
        lines.append(current_ask)
        colors.append('tab:blue')

    ax = fig.gca()

    ymin, ymax = ax.get_ylim()
    ax.vlines(x=lines, ymin=ymin, ymax=ymax, colors=colors, ls='-', lw=2)

    fig_canvas.draw()


def draw_account_balances(
        base_currency_account: AccountInfo,
        quote_currency_account: AccountInfo,
        inventory,
        account_tv: Treeview):
    """
    Draws the current account balances in a table for the renderer

    :param base_currency_account: The currency account being traded e.g. BTC or ETH
    :param quote_currency_account: The currency account being exchanged e.g. USDT or USDC
    :param inventory: The current position held in the strategy
    :param account_tv: The table rendering our values
    """
    account_tv.delete(*account_tv.get_children())

    if base_currency_account is None or quote_currency_account is None:
        return

    account_tv.insert(parent='', index=0, iid=str(0), text='', values=(
        base_currency_account.currency,
        base_currency_account.total_balance,
        base_currency_account.available_balance,
        base_currency_account.holds_balance,
        quote_currency_account.currency,
        quote_currency_account.total_balance,
        quote_currency_account.available_balance,
        quote_currency_account.holds_balance,
        inventory
    ))
