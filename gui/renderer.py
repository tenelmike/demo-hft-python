
import asyncio
import time
import logging

from tkinter import TclError

from gui.frontend import (draw_bids, draw_asks, draw_fills, draw_account_balances,
                          draw_order_book_depth, draw_current_quote, launch_gui)


# Create a logger object
logger = logging.getLogger('system')

# Set the log level to DEBUG so that all messages are logged
logger.setLevel(logging.DEBUG)


def gui_cb(renderer):
    """
    Uses the tk function after to schedule a new order using a callback. Also calls the main update functions for
    the framework's windows

    :param renderer: The OrderBookRenderer
    :return: void
    """
    renderer.main_window.update_idletasks()
    renderer.main_window.update()


class OrderBookRenderer:
    """
    Uses the tkinter GUI framework to render an OrderBook
    """

    def __init__(self):
        """
        Initializes the renderer
        """
        # if the renderer is running
        self.run = True

        # The state class storing program info like the order book and account info
        self.state = None

        # Callback to be run on shutdown when the user presses the X button on the GUI
        self.shutdown_cb = None

        # initializes the gui components using the helper function
        (self.main_window, self.bid_treeview, self.ask_treeview, self.fill_treeview, self.account_treeview,
         self.fig, self.fig_canvas) = launch_gui()

    def set_state(self, state):
        """
        Sets the state reference so the GUI can render values from the program
        """
        self.state = state

    def set_shutdown_callback(self, callback):
        """
        Sets a callback that will be called when the user presses the X button on the GUI window

        :param callback: The callback to run
        """
        self.shutdown_cb = callback

    def terminate(self):
        """
        Function allowing the renderer to be terminated gracefully by another thread or callback
        :return: void
        """
        self.run = False

    # async def loop(self, gui_callbacks=gui_cb):
    def loop(self, gui_callbacks=gui_cb):
        """
        The main renderer loop

        :param gui_callbacks: Any callbacks the gui needs to run. For tkinter there are some
        :return: void
        """
        while self.run:

            if self.shutdown_cb is not None:
                self.main_window.protocol("WM_DELETE_WINDOW", lambda: self.shutdown_cb(self))

            try:
                # 60fps
                # await asyncio.sleep(0.016)
                time.sleep(0.016)

                if self.state is not None:

                    draw_fills(self.state.trade_fills, self.fill_treeview)
                    draw_bids(self.state.order_book.bids, self.bid_treeview)
                    draw_asks(self.state.order_book.asks, self.ask_treeview)
                    draw_order_book_depth(self.state.order_book, self.fig, self.fig_canvas)
                    draw_current_quote(self.state.current_bid, self.state.current_ask,
                                       self.fig, self.fig_canvas)

                    inventory = float(self.state.inventory_q)

                    draw_account_balances(self.state.base_currency_account_info,
                                          self.state.quote_currency_account_info,
                                          inventory, self.account_treeview)

                    gui_callbacks(self)
            except TclError:
                # if there are problems (like the gui X button being pressed) gracefully shut down the renderer
                self.terminate()
            except asyncio.TimeoutError:
                # call the gui callbacks if specified
                if gui_callbacks is not None:
                    gui_callbacks(self)
        logger.info('Renderer Shutdown, Destroying GUI')
        self.main_window.destroy()
