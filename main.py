
import os
from system import main_fcn
from dotenv import load_dotenv

load_dotenv()  # Load the .env file into the environment
SYMBOL = os.getenv('SYMBOL')


if __name__ == '__main__':
    main_fcn(SYMBOL)
