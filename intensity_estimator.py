"""
Class for estimating the order book intensity parameters A and k
"""
from numba import jit
import numpy as np
from scipy.optimize import curve_fit
from decimal import Decimal


@jit(nopython=True)
def find_price_level(price_to_bin: float, price_levels: np.ndarray):
    """
    numba function to find the index of a price in a numpy array of prices really fast

    :param price_to_bin: the price we are searching for the index of the price level bin it belongs
    :param price_levels: the array of price levels
    :return:
    """
    # loop from zero slowly increasing in price
    for i in range(len(price_levels)):

        # when we find a price we are less than, we know the bin it belongs
        if price_to_bin < price_levels[i]:
            return i - 1
    return -1


@jit(nopython=True)
def find_smallest_and_largest_timestamps(timestamps: np.ndarray):

    smallest = np.inf
    largest = 0
    for ts in timestamps:
        if ts < smallest:
            smallest = ts
        if ts > largest:
            largest = ts
    return smallest, largest


class IntensityEstimator:

    def __init__(self, price_increment: Decimal, buffer_size=1000):

        self.price_increment = price_increment
        self.buffer_size = buffer_size
        self.total_trade_amount = Decimal('0')
        self.trade_amounts = np.asarray([Decimal('0')
                                         for _ in range(int(self.buffer_size))], dtype=np.float64)
        self.price_levels = np.asarray([i * self.price_increment
                                        for i in range(int(self.buffer_size))], dtype=np.float64)
        self.timestamps = np.zeros(buffer_size, dtype=np.int64)
        self.last_sequence_number = 0
        self.buffer_index = 0

        self.k = 0.0
        self.A = 0.0
        self.delta_t = 0.0
        self.estimated = False

    def add_sample_to_buffer(self, trade_price: Decimal, midpoint_price: Decimal,
                             trade_amount: Decimal, timestamp_ns: int, sequence_number: int):

        # the avellaneda-stoikov model assumes brownian motion and not geometric brownian motion
        simple_delta = np.abs(trade_price - midpoint_price)

        # find the index and store the amount at that price level
        index = find_price_level(float(simple_delta), self.price_levels)
        self.trade_amounts[index] += float(trade_amount)
        self.timestamps[self.buffer_index] = timestamp_ns

        # update the total trade amount so we can normalize later
        self.total_trade_amount += trade_amount

        # increment the counter and update the sequence number
        self.buffer_index += 1
        self.buffer_index = self.buffer_index % self.buffer_size
        self.last_sequence_number = sequence_number

    def check_if_buffer_full(self):
        return self.buffer_index % self.buffer_size == 0

    def estimate_parameters(self):

        params = curve_fit(lambda t, a, b: a * np.exp(-b * t),
                           self.price_levels,
                           self.trade_amounts,
                           p0=(self.A, self.k),
                           method='dogbox',
                           bounds=([0, 0], [np.inf, np.inf]))

        optimal_values = params[0]
        _covariance_matrix = params[1]

        # calculate the number of seconds passed collecting samples
        smallest_ts, largest_ts = find_smallest_and_largest_timestamps(self.timestamps)

        # convert to seconds
        self.delta_t = (largest_ts - smallest_ts) / 1000000000.0

        # scale the amount traded by the time elapsed so A is in units of X/s
        _A = optimal_values[0] / self.delta_t

        self.A = Decimal(str(_A))
        self.k = Decimal(str(optimal_values[1]))

        self.estimated = True

        return self.A, self.k
