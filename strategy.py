
from decimal import Decimal
import numpy as np
import asyncio
import os
from typing import Union
from dotenv import load_dotenv

from leaky_bucket import LeakyBucketRateLimiter
from state_manager import StateManager
from kucoin.rest.rest_response_parsing import wait_and_parse_create_margin_limit_order


load_dotenv()  # Load the .env file into the environment
A_PARAM = Decimal(os.getenv('A_PARAM'))
K_PARAM = Decimal(os.getenv('K_PARAM'))
GAMMA_PARAM = Decimal(os.getenv('GAMMA_PARAM'))
MU_PARAM = Decimal(os.getenv('MU_PARAM'))
SIGMA_PARAM = Decimal(os.getenv('SIGMA_PARAM'))
MIN_Q_PARAM = Decimal(os.getenv('MIN_Q_PARAM'))
MAX_Q_PARAM = Decimal(os.getenv('MAX_Q_PARAM'))
REST_PERIOD = float(os.getenv('REST_PERIOD'))
TEST_MODE = (os.getenv('TEST_MODE', 'False') == 'True')


def avellaneda_stoikov_log_term(gamma: Decimal, k: Decimal):
    """
    Calculates the log term of the avellaneda-stoikov model.

    Gamma values closer to 1 adjusts the spread based on inventory. Closer to 0 keeps bid/ask symmetrical
    k values that are large assume that the order book is dense and will keep the spread smaller. Small k
    values assume the order book has little liquidity and keeps the spread wider

    :param gamma: risk factor in the model, between 0 (risky) and 1 (low risk).
    :param k: Order book liquidity parameter
    :return: The value of the natural log term in the equation
    """
    return (Decimal(1.0) / gamma) * Decimal(np.log(float(Decimal(1.0) + (gamma / k))))


def avellaneda_stoikov_sqrt_term(gamma: Decimal, k: Decimal, sigma: Decimal, a: Decimal):
    """
    Calculates the square root term of the avellaneda-stoikov model.

    Gamma values closer to 1 adjusts the spread based on inventory. Closer to 0 keeps bid/ask symmetrical
    k values that are large assume that the order book is dense and will keep the spread smaller. Small k
    values assume the order book has little liquidity and keeps the spread wider

    sigma represents the standard deviation of the price process, larger values make the spread wider
    a represents the scaling constant in the model of arrival probability fit to the incoming fill trades.
    Larger values of a make the spread smaller and smaller values make it wider

    :param gamma: risk factor in the model, between 0 (risky) and 1 (low risk).
    :param k: Order book liquidity parameter
    :param sigma: standard deviation of process parameter
    :param a: trade arrival scaling parameter
    :return: the square root term
    """
    return Decimal(np.sqrt(float(((gamma * sigma ** Decimal(2.0)) / (Decimal(2.0) * k * a)) * (
            Decimal(1.0) + (gamma / k)) ** (Decimal(1.0) + (k / gamma)))))


def avellaneda_stoikov_bid_drift_term(mu: Decimal, gamma: Decimal, sigma: Decimal, q: Decimal):
    """
    Calculates the drift term for bids of the avellaneda-stoikov model.

    mu represents the drift of the price process

    Gamma values closer to 1 adjusts the spread based on inventory. Closer to 0 keeps bid/ask symmetrical
    sigma represents the standard deviation of the price process, larger values make the spread wider

    q represents the inventory and can be positive (long) or negative (short) the asset. In this model there
    is a configured min and max inventory to make the proof work and limit risk

    :param mu: the drift of the price process
    :param gamma: risk factor in the model, between 0 (risky) and 1 (low risk).
    :param sigma: standard deviation of process parameter
    :param q: the inventory (number of shares or units of the asset held)
    :return: the drift term of the model
    """
    return (-mu / (gamma * sigma ** Decimal(2.0))) + ((Decimal(2.0) * q + Decimal(1.0)) / Decimal(2.0))


def avellaneda_stoikov_ask_drift_term(mu: Decimal, gamma: Decimal, sigma: Decimal, q: Decimal):
    """
    Calculates the drift term for asks of the avellaneda-stoikov model.

    mu represents the drift of the price process

    Gamma values closer to 1 adjusts the spread based on inventory. Closer to 0 keeps bid/ask symmetrical
    sigma represents the standard deviation of the price process, larger values make the spread wider

    q represents the inventory and can be positive (long) or negative (short) the asset. In this model there
    is a configured min and max inventory to make the proof work and limit risk

    :param mu: the drift of the price process
    :param gamma: risk factor in the model, between 0 (risky) and 1 (low risk).
    :param sigma: standard deviation of process parameter
    :param q: the inventory (number of shares or units of the asset held)
    :return: the drift term of the model
    """
    return (mu / (gamma * sigma ** Decimal(2.0))) - ((Decimal(2.0) * q - Decimal(1.0)) / Decimal(2.0))


def calculate_bid(gamma: Decimal, k: Decimal, mu: Decimal, sigma: Decimal, q: Decimal, a: Decimal):
    """
    Calculates the bid price of the avellaneda-stoikov model.

    Gamma values closer to 1 adjusts the spread based on inventory. Closer to 0 keeps bid/ask symmetrical
    k values that are large assume that the order book is dense and will keep the spread smaller. Small k
    values assume the order book has little liquidity and keeps the spread wider

    mu represents the drift of the price process
    sigma represents the standard deviation of the price process, larger values make the spread wider

    q represents the inventory and can be positive (long) or negative (short) the asset. In this model there
    is a configured min and max inventory to make the proof work and limit risk

    a represents the scaling constant in the model of arrival probability fit to the incoming fill trades.
    Larger values of a make the spread smaller and smaller values make it wider

    :param gamma: risk factor in the model, between 0 (risky) and 1 (low risk).
    :param k: Order book liquidity parameter
    :param mu: the drift of the price process
    :param sigma: standard deviation of process parameter
    :param q: the inventory (number of shares or units of the asset held)
    :param a: trade arrival scaling parameter
    :return: the bid price
    """
    log_term = avellaneda_stoikov_log_term(gamma, k)
    drift_term = avellaneda_stoikov_bid_drift_term(mu, gamma, sigma, q)
    sqrt_term = avellaneda_stoikov_sqrt_term(gamma, k, sigma, a)

    return log_term + drift_term * sqrt_term


def calculate_ask(gamma: Decimal, k: Decimal, mu: Decimal, sigma: Decimal, q: Decimal, a: Decimal):
    """
    Calculates the ask price of the avellaneda-stoikov model.

    Gamma values closer to 1 adjusts the spread based on inventory. Closer to 0 keeps bid/ask symmetrical
    k values that are large assume that the order book is dense and will keep the spread smaller. Small k
    values assume the order book has little liquidity and keeps the spread wider

    mu represents the drift of the price process
    sigma represents the standard deviation of the price process, larger values make the spread wider

    q represents the inventory and can be positive (long) or negative (short) the asset. In this model there
    is a configured min and max inventory to make the proof work and limit risk

    a represents the scaling constant in the model of arrival probability fit to the incoming fill trades.
    Larger values of a make the spread smaller and smaller values make it wider

    :param gamma: risk factor in the model, between 0 (risky) and 1 (low risk).
    :param k: Order book liquidity parameter
    :param mu: the drift of the price process
    :param sigma: standard deviation of process parameter
    :param q: the inventory (number of shares or units of the asset held)
    :param a: trade arrival scaling parameter
    :return: the ask price
    """
    log_term = avellaneda_stoikov_log_term(gamma, k)
    drift_term = avellaneda_stoikov_ask_drift_term(mu, gamma, sigma, q)
    sqrt_term = avellaneda_stoikov_sqrt_term(gamma, k, sigma, a)

    return log_term + drift_term * sqrt_term


def calculate_spread(gamma: Decimal, k: Decimal, sigma: Decimal, a: Decimal):
    """
    Calculates the width of the price spread of the avellaneda-stoikov model.

    Gamma values closer to 1 adjusts the spread based on inventory. Closer to 0 keeps bid/ask symmetrical
    k values that are large assume that the order book is dense and will keep the spread smaller. Small k
    values assume the order book has little liquidity and keeps the spread wider

    sigma represents the standard deviation of the price process, larger values make the spread wider

    a represents the scaling constant in the model of arrival probability fit to the incoming fill trades.
    Larger values of a make the spread smaller and smaller values make it wider

    :param gamma: risk factor in the model, between 0 (risky) and 1 (low risk).
    :param k: Order book liquidity parameter
    :param sigma: standard deviation of process parameter
    :param a: trade arrival scaling parameter
    :return: the width of the spread (ask - bid)
    """
    log_term = avellaneda_stoikov_log_term(gamma, k)
    sqrt_term = avellaneda_stoikov_sqrt_term(gamma, k, sigma, a)

    return Decimal(2.0) * log_term + sqrt_term


class StrategyRunner:
    """
    A class that runs the avellaneda-stoikov market making model
    """

    def __init__(self):
        """
        Initialize the strategy with the parameters from the .env file
        """
        self.state = None

        self.risk_factor_gamma = GAMMA_PARAM
        self.liqidity_exponent_k = K_PARAM
        self.liqidity_scaling_A = A_PARAM
        self.price_drift_mu = MU_PARAM
        self.price_volatility_sigma = SIGMA_PARAM

        self.create_order_rate_limiter: Union[LeakyBucketRateLimiter, None] = None

    def set_state(self, state: StateManager):
        """
        Sets the state reference so the strategy can interact with the rest of the system
        """
        self.state = state
        rest_client = self.state.rest_client

        # place orders rate limit is 45/3s or 15 per second
        self.create_order_rate_limiter = LeakyBucketRateLimiter(
            rest_client.loop, rest_client.create_margin_limit_order,
            wait_and_parse_create_margin_limit_order, refill_rate=15.0, max_tokens=45.0)

    async def loop(self):
        """
        The main strategy loop

        :return: void
        """
        while self.state is None:
            await asyncio.sleep(REST_PERIOD)

        while self.state.strategy_keep_running:

            cancel_time = REST_PERIOD
            await asyncio.sleep(cancel_time)

            # unpack what we need from the state
            margin_info = self.state.margin_info
            total_balance = self.state.base_currency_account_info.total_balance
            hold_balance = self.state.quote_currency_account_info.holds_balance
            symbol = self.state.symbol_info.symbol
            min_size = self.state.symbol_info.base_min_size
            price_increment = self.state.symbol_info.price_increment
            order_book = self.state.order_book

            # claculate the inventory, factoring margin borrowed for entering short positions
            total_owed = Decimal(margin_info.borrowed_liability + margin_info.accrued_interest)
            self.state.inventory_q = total_balance - total_owed

            # calculate the bid and ask according to the model
            mid_price = Decimal(order_book.get_midpoint_price())

            self.state.current_bid = mid_price - calculate_bid(self.risk_factor_gamma, self.liqidity_exponent_k,
                                                               self.price_drift_mu, self.price_volatility_sigma,
                                                               self.state.inventory_q, self.liqidity_scaling_A)

            self.state.current_ask = mid_price + calculate_ask(self.risk_factor_gamma, self.liqidity_exponent_k,
                                                               self.price_drift_mu, self.price_volatility_sigma,
                                                               self.state.inventory_q, self.liqidity_scaling_A)

            # If we hit our position limits, have no balance, or everything is bet do not quote
            if self.state.inventory_q >= MAX_Q_PARAM:
                self.state.current_bid = None
            if self.state.inventory_q <= MIN_Q_PARAM or total_balance <= Decimal(0.0) or total_balance == hold_balance:
                self.state.current_ask = None

            # If we are in test mode don't send orders to the server
            if not TEST_MODE:

                if self.state.current_bid is not None:

                    self.state.current_bid = self.state.current_bid.quantize(price_increment)

                    await self.create_order_rate_limiter.schedule_request(
                        symbol, 'buy', str(self.state.current_bid), str(min_size), 'GTT', round(cancel_time * 0.99, 6)
                    )
                if self.state.current_ask is not None:

                    self.state.current_ask = self.state.current_ask.quantize(price_increment)

                    await self.create_order_rate_limiter.schedule_request(
                        symbol, 'sell', str(self.state.current_ask), str(round(float(min_size), 6)),
                        'GTT', round(cancel_time * 0.99, 6)
                    )

        # when we are done, clear out any orders we are waiting for responses for
        await self.create_order_rate_limiter.terminate()
