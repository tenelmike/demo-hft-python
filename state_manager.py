"""
A class for storing the program's state information
"""
from decimal import Decimal

from kucoin.kucoin_dataclasses import SymbolInfo, TickerInfo, WebsocketInfo, AccountInfo, MarginInfo
from kucoin.rest.client import KucoinRestClient
from trade_fills_log import TradeFills


class StateManager:
    """
    A class for storing the program's state information
    """

    def __init__(self,
                 symbol_info: SymbolInfo,
                 ticker_info: TickerInfo,
                 websocket_info: WebsocketInfo,
                 base_currency_account: AccountInfo,
                 quote_currency_account: AccountInfo,
                 margin_info: MarginInfo,
                 rest_client: KucoinRestClient):
        """
        Initializes the state manager

        :param symbol_info: The SymbolInfo
        :param ticker_info: The TickerInfo
        :param websocket_info: The WebsocketInfo
        :param base_currency_account: The AccountInfo for the base currency
        :param quote_currency_account: The AccountInfo for the quote currency
        :param margin_info: The MarginInfo for the margin account
        :param rest_client: The KucoinRestClient
        """
        # information about the trading symbol like price_increment, quote currency, base currency, etc.
        self.symbol_info = symbol_info

        # stores information about the ticker
        self.ticker_info = ticker_info

        # stores info about configuring the websocket
        self.websocket_info = websocket_info

        # stores account balances about the base currency being traded
        self.base_currency_account_info = base_currency_account

        # stores account balances about the quote currency
        self.quote_currency_account_info = quote_currency_account

        # stores info about the margin account
        self.margin_info = margin_info

        # The order book state
        self.order_book = None

        # The log of trades that have been filled
        self.trade_fills = TradeFills()

        # so other parts of the program have a reference and can make requests, like the strategy
        # or the gui if we add any interactions where buttons make requests in the future
        self.rest_client = rest_client

        # If the websocket loop should keep running
        self.websocket_keep_running = True

        # If the trading strategy loop should keep running
        self.strategy_keep_running = True

        # The current bid price being quoted
        self.current_bid = None

        # The current ask price being quoted
        self.current_ask = None

        # The current inventory position
        self.inventory_q = Decimal(0.0)
