"""
Implements the Leaky Bucket Algorithm to rate limit requests to an API at a configurable rate.
"""
import asyncio
import time
import uuid

from typing import Callable


class LeakyBucketRateLimiter:
    """
    Implements the Leaky Bucket Algorithm to rate limit requests to an API at a configurable rate.

    The class itself is agnostic to the underlying API since it implements this behavior using a user specified
    callback function. The user sets the function they would like to have rate limited and when requests are
    scheduled with the class any args and kwargs parameters are passed through to the callback.

    When requests are scheduled, a unique id is returned to the calling coroutine. When the caller would like to
    get the response, the unique id is passed to the get_response function
    """

    def __init__(self, loop, request_callback: Callable, response_parse=None, refill_rate=50.0, max_tokens=500.0):
        """
        Initializes the Leaky Bucket algorithm using a given event loop and request callback to rate limit

        :param loop: The event loop for the asyncio event queue to schedule coroutines
        :param request_callback: The callback function that will be rate limited
        :param refill_rate: The rate of requests/second that the rate limiter limits
        """
        # the rate of requests per second
        self.refill_rate = refill_rate

        # the max number of tokens
        self.max_tokens = max_tokens

        # sleep period in seconds, set to the nyquist sampling period
        self.sleep_period = (1.0 / self.refill_rate) / 2.0

        # The number of tokens that counts if the bucket can set a request
        self.tokens = 0.0

        # flag controlling whether the rate limiting coroutine continues to run
        self.keep_running = True

        # the asyncio event loop
        self.loop = loop

        # the queue for scheduling events to be paced by the leaky bucket
        self.queue = asyncio.Queue()

        # The last time in seconds the bucket was refilled
        self.last_refill_time = time.time()

        # the function to call when the rate limiting algorithm makes a request
        self.request_callback = request_callback

        self.response_parse = response_parse

        # mapping for storing API responses that the coroutine scheduling requests can use to get their response
        self.response_mapping = {}

    async def terminate(self):
        """
        Terminates the coroutine loop that performs the rate limiting leaky bucket algorithm
        """
        self.keep_running = False
        # self.rate_limiter_task.cancel()

        # process any waiting tasks
        try:
            for unique_id, response in self.response_mapping.items():
                _resp_data = await self.response_parse(response)
        except Exception:
            pass

    async def schedule_request(self, *args, **kwargs):
        """
        Schedules a rate-limited request and passes the args and kwargs to the callback function that implements
        making the API request. A unique id is created and returned to the caller to identify the response info
        when it is returned by the API

        :param args: The API request function args
        :param kwargs: The API request function kwargs
        :return: A unique id to identify API response
        """
        unique_id = str(uuid.uuid4())

        self.queue.put_nowait((unique_id, args, kwargs))

        return unique_id

    async def get_response(self, unique_id, wait_period=0.001):
        """
        Gets a response from the results mapping for the unique id. If one is not yet available the calling
        coroutine will block until a response is available. The time to wait before checking again is configurable

        :param unique_id: The unique id of the request that was made
        :param wait_period: The amount of time to block before checking for a response again (Default 1ms)
        :return: The response from the API call for the given unique id
        """
        # Wait for the entry to be available in the results mapping
        while unique_id not in self.response_mapping:
            await asyncio.sleep(wait_period)

        # Retrieve the response from the results mapping
        return self.response_mapping[unique_id]

    def _refill_tokens(self):
        """
        Refills some tokens based on the passage of time at the leaky bucket rate
        """
        current_time = time.time()
        time_since_last_refill = current_time - self.last_refill_time
        new_tokens = time_since_last_refill * self.refill_rate
        # print('time: ', time_since_last_refill, ' nt: ', new_tokens)

        self.tokens = self.tokens + new_tokens

        if self.tokens >= self.max_tokens:
            self.tokens = self.max_tokens

        self.last_refill_time = current_time

    async def process_idle_tasks_or_sleep(self):

        try:
            # check if any responses need to be parsed
            to_remove = []
            for unique_id, response in self.response_mapping.items():
                if response.done():
                    _resp_data = await self.response_parse(response)
                    to_remove.append(unique_id)
            for unique_id in to_remove:
                del self.response_mapping[unique_id]
            current_time = time.time()
            time_since_last_refill = current_time - self.last_refill_time
            # Pace the requests at a constant rate
            await asyncio.sleep(self.sleep_period - time_since_last_refill)
        except RuntimeError:
            return

    async def handle_requests(self):
        """
        Coroutine that implements the leaky bucket algorithm and sends requests to the API at the specified rate.
        The algorithm builds tokens over time and if enough are accumulated the algorithm allows for bursted
        requests. Tokens are drained as requests are made.

        Responses are stored in the response mapping according to the request's unique id so that they can
        be retrieved by the request scheduling coroutine when it is prepared to await for the response info
        """
        while self.keep_running:

            # refill the leaky bucket tokens
            self._refill_tokens()

            # check if the rate limiter allows requests
            if self.tokens > 0:
                try:
                    unique_id, args, kwargs = self.queue.get_nowait()
                    # print(unique_id, args, kwargs)
                except asyncio.QueueEmpty:
                    # no requests are scheduled, allow tokens to accumulate
                    await self.process_idle_tasks_or_sleep()
                    continue

                # make the request, store the result in the results map and reduce the token count
                response = await self.request_callback(*args, **kwargs)
                # print('requested')
                self.response_mapping[unique_id] = response
                self.tokens -= 1.0
            else:
                await self.process_idle_tasks_or_sleep()
