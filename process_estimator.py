"""
Class for estimating the price process parameters mu and sigma
"""
from scipy.stats import norm
import numpy as np
from decimal import Decimal


class ProcessEstimator:

    def __init__(self, buffer_size=1000):

        self.buffer_size = buffer_size
        self.simple_returns = np.zeros(buffer_size, dtype=np.float64)
        self.buffer_index = 0
        self.last_price = None
        self.last_sequence_number = 0

        self.mu = 0.0
        self.sigma = 0.0
        self.estimated = False

    def add_sample_to_buffer(self, trade_price: Decimal, sequence_number: int):

        if self.last_price is not None:

            simple_return = (trade_price - self.last_price) / self.last_price

            self.simple_returns[self.buffer_index] = simple_return
            self.buffer_index += 1
            self.buffer_index = self.buffer_index % self.buffer_size

        self.last_price = trade_price
        self.last_sequence_number = sequence_number

    def check_if_buffer_full(self):
        return self.buffer_index % self.buffer_size == 0

    def estimate_parameters(self):
        [self.mu, self.sigma] = norm.fit(self.simple_returns, loc=0.0)
        self.estimated = True
