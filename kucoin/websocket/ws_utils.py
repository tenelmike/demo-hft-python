
from decimal import Decimal
import logging
import asyncio

from state_manager import StateManager


# Create a logger object
logger = logging.getLogger('kucoin.websocket')

# Set the log level to DEBUG so that all messages are logged
logger.setLevel(logging.DEBUG)


async def message_queue_callback(event, queue: asyncio.Queue):
    """
    Callback that adds an event to the message queue when it is received (non-blocking)

    :param event: The event to add to the queue
    :param queue: The queue we are adding the event to
    """
    if queue is not None:
        queue.put_nowait(event)


async def parse_order_book_update(order_msg, state: StateManager):
    """
    Parses an order book update message received on the websocket

    :param order_msg: The message received as a json dict
    :param state: The state manager containing the state info for the program
    """
    order_data = order_msg['data']

    sequence_start = order_data['sequenceStart']
    sequence_end = order_data['sequenceEnd']

    # If the sequence numbers are newer than the order book snapshot, apply the update to the book
    if sequence_start <= (state.order_book.sequence_number + 1) and (
            sequence_end > state.order_book.sequence_number):

        # get the bid and ask updates
        bid_update = order_data['changes']['bids']
        ask_update = order_data['changes']['asks']

        for bid in bid_update:
            state.order_book.bids.update_amount_at_price_level(Decimal(bid[0]), Decimal(bid[1]))

        for ask in ask_update:
            state.order_book.asks.update_amount_at_price_level(Decimal(ask[0]), Decimal(ask[1]))

        # update the sequence number for the next message
        state.order_book.sequence_number = sequence_end


async def parse_match_message(match_msg, state: StateManager):
    """
    Parses a match message received on the websocket

    :param match_msg: The match message received
    :param state: The state manager containing the state info for the program
    """
    match_data = match_msg['data']

    side = match_data['side']
    price = match_data['price']
    quantity = match_data['size']
    trade_id = match_data['tradeId']
    taker_id = match_data['takerOrderId']
    maker_id = match_data['makerOrderId']
    timestamp_ns = int(match_data['time'])

    # record the match in the log of trades that were filled from the order book
    state.trade_fills.create_match_fill(timestamp_ns, side, price, quantity, trade_id, maker_id, taker_id)


async def parse_account_balance_update(acct_balance_msg, state: StateManager):
    """
    Parses a websocket message that updates the account balances.

    Example format:
    "total": "88", // total balance
    "available": "88", // available balance
    "availableChange": "88", // the change of available balance
    "currency": "KCS", // currency
    "hold": "0", // hold amount
    "holdChange": "0", // the change of hold balance
    "relationEvent": "trade.setted", //relation event
    "relationEventId": "5c21e80303aa677bd09d7dff", // relation event id
    "relationContext": {
    "symbol":"BTC-USDT",
    "tradeId":"5e6a5dca9e16882a7d83b7a4", // the trade Id when order is executed
    "orderId":"5ea10479415e2f0009949d54"

    :param acct_balance_msg: The websocket message dict
    :param state: The state manager containing the state info for the program
    """
    account_data = acct_balance_msg['data']

    total_balance = Decimal(account_data['total'])
    available_balance = Decimal(account_data['available'])
    _available_change = Decimal(account_data['availableChange'])
    currency = str(account_data['currency'])
    hold_balance = Decimal(account_data['hold'])
    _hold_change = Decimal(account_data['holdChange'])

    if state.base_currency_account_info is not None:

        if state.base_currency_account_info.currency == currency:
            state.base_currency_account_info.total_balance = total_balance
            state.base_currency_account_info.available_balance = available_balance
            state.base_currency_account_info.holds_balance = hold_balance

            # print(websocket.base_currency_account_info)

    if state.quote_currency_account_info is not None:

        if state.quote_currency_account_info.currency == currency:
            state.quote_currency_account_info.total_balance = total_balance
            state.quote_currency_account_info.available_balance = available_balance
            state.quote_currency_account_info.holds_balance = hold_balance

            # print(websocket.quote_currency_account_info)


async def message_processing_coroutine(websocket, state: StateManager):
    """
    The coroutine that waits for messages in the queue and processes them depending on what kind of message it is

    :param websocket: The websocket receiving the messages
    :param state: The state manager containing the state info for the program
    """
    # Coroutine runs as long as the websocket is connected
    while state.websocket_keep_running:

        # Blocks until a message is received
        try:
            socket_message = await asyncio.wait_for(websocket.message_queue.get(), timeout=1.0)
        except asyncio.TimeoutError:
            continue

        # Parse the message depending on the type
        if '/market/match' in str(socket_message['topic']):
            await parse_match_message(socket_message, state)
        elif '/market/level2' in str(socket_message['topic']):
            await parse_order_book_update(socket_message, state)
        elif '/account/balance' in str(socket_message['topic']):
            # print(socket_message)
            await parse_account_balance_update(socket_message, state)

    logger.info('Message Processing Coroutine Terminated')
