
import json
import logging
import os
import time
import asyncio
import random
from dotenv import load_dotenv

from typing import Dict

import websockets as ws

from state_manager import StateManager
from kucoin.websocket.ws_utils import message_queue_callback
from kucoin.utils import make_kucoin_unique_id_str, make_kucoin_websocket_url

load_dotenv()  # Load the .env file into the environment
KUCOIN_API_SECRET = os.getenv('KUCOIN_API_SECRET')
KUCOIN_API_KEY = os.getenv('KUCOIN_API_KEY')
KUCOIN_API_PASSWORD = os.getenv('KUCOIN_API_PASSWORD')

# Create a logger object
logger = logging.getLogger('kucoin.websocket')

# Set the log level to DEBUG so that all messages are logged
logger.setLevel(logging.DEBUG)

# Create a handler for WebSocket messages
websocket_handler = logging.StreamHandler()
websocket_handler.setLevel(logging.INFO)
websocket_formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
websocket_handler.setFormatter(websocket_formatter)
logger.addHandler(websocket_handler)

# Create a handler for HTTP and system errors
error_handler = logging.StreamHandler()
error_handler.setLevel(logging.ERROR)
error_formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
error_handler.setFormatter(error_formatter)
logger.addHandler(error_handler)

# create a handler for debug messages
debug_handler = logging.StreamHandler()
debug_handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
debug_handler.setFormatter(formatter)
logger.addHandler(debug_handler)

MAXIMUM_RECONNECTS = 5
MAXIMUM_RECONNECT_SECONDS = 60
MINIMUM_RECONNECT_WAIT = 0.1


def exponential_backoff_reconnect_wait(num_tries):
    """
    Calculates exponentially longer time to wait between reconnection attempts

    :param num_tries: The number of times we have tried to reconnect
    """
    exponent = 2.0 ** num_tries
    return round(random.random() * min(MAXIMUM_RECONNECT_SECONDS, exponent - 1) + 1)


class KuCoinWebsocket:

    def __init__(self, async_evt_loop, state: StateManager, queue_callback=message_queue_callback, private_ws=False):
        """
        Initializes the KuCoinWebsocket object

        :param async_evt_loop: The asyncio event loop to use
        :param state: The program state manager containing the websocket configuration information
        :param queue_callback: The coroutine to call when a message is received
        :param private_ws: Whether the websocket is for private account data
        """
        # the asyncio event loop that schedules events
        self.loop = async_evt_loop

        # the info we gathered about the websocket from the initial REST request
        self.state = state

        # called when the websocket receives a message
        self.message_queue_callback = queue_callback

        # whether the website uses only public data or if it uses private account data
        self.private_ws = private_ws

        # creates a future that will run the loop
        self.connection = asyncio.ensure_future(self.run(), loop=self.loop)

        # convert timeout to seconds, divide by 2, and ping 1 second faster
        # so even if 1 message is missed 2 messages are in the window
        self.ping_timeout_s = ((self.state.websocket_info.ping_timeout / 1000) / 2) - 1

        # set the last ping time to when the object is constructed since the
        # connection is not yet open
        self.last_ping_time = time.time()

        # The websocket object that will be created upon connecting
        self.websocket = None

        # The queue to store websocket messages for later processing
        self.message_queue = asyncio.Queue()

        # The number of times we have tried to reconnect
        self.reconnect_count = 0

        self.closed = False

    def terminate(self) -> None:
        """
        Gracefully shuts down the websocket

        :return: None
        """
        self.state.websocket_keep_running = False

    def close_websocket(self) -> None:
        """
        Closes the websocket and logs any errors encountered in the process

        :return: None
        """
        try:
            self.connection.cancel()
            logger.info('Websocket Connection Closed')
        except asyncio.CancelledError:
            logger.debug(f'Coroutine was cancelled.')
        except ws.ConnectionClosed:
            logger.debug(f'Connection was already closed when shutdown attempted.')

    async def reconnect(self):
        """
        Reconnects the websocket if it becomes disconnected
        """
        if not self.state.websocket_keep_running:
            return

        # close any existing connection
        self.close_websocket()

        # increment our counter before trying
        self.reconnect_count += 1
        if self.reconnect_count < MAXIMUM_RECONNECTS:
            logger.debug(
                f'Websocket reconnecting, {MAXIMUM_RECONNECTS - self.reconnect_count} number of tried remaining')

            # gets the amount of time to wait between reconnection attempts
            reconnect_wait = exponential_backoff_reconnect_wait(self.reconnect_count)

            logger.debug(f'Waiting for: {reconnect_wait} before reconnecting')

            # sleep until it is time
            await asyncio.sleep(reconnect_wait)

            logger.debug(f'Attempting websocket reconnection')

            self.connection = asyncio.ensure_future(self.run(), loop=self.loop)

            logger.debug('Websocket reconnected. Subscribing to order book updates')

            # request the subscriptions we are interested in on the websocket
            await self.subscribe(f'/market/level2:{self.state.symbol_info.symbol}', private_msg=False)
            await self.subscribe(f'/market/match:{self.state.symbol_info.symbol}', private_msg=False)
            await self.subscribe(f'/account/balance', private_msg=True)

            await asyncio.sleep(1)
        else:
            logger.error(f'Websocket reconnecttion limit reached.')
            self.state.websocket_keep_running = False

    async def run(self) -> None:
        """
        Runs the main websocket loop

        :return: None
        """
        async with ws.connect(make_kucoin_websocket_url(
                self.state.websocket_info, self.private_ws), ssl=self.state.websocket_info.encrypt) as kucoin_ws:

            logger.info('Starting Websocket Loop')

            # save the reference to the open websocket so we can use it elsewhere in the class
            self.websocket = kucoin_ws
            self.last_ping_time = time.time()

            while self.state.websocket_keep_running:
                try:
                    # check if pings are needed
                    time_since_ping = (time.time() - self.last_ping_time)
                    if self.ping_timeout_s <= time_since_ping:
                        logger.info(f'Time since last ping: {time_since_ping}')
                        await self.send_ping_message()

                    try:
                        # waits for an event on the websocket, parses the json, and passes the event to our
                        # custom coroutine callback for processing
                        event = await asyncio.wait_for(self.websocket.recv(), timeout=self.ping_timeout_s)
                        event_json = json.loads(event)

                        if event_json['type'] == 'message':
                            await self.message_queue_callback(event_json, queue=self.message_queue)

                    except asyncio.TimeoutError:
                        await self.reconnect()
                    except asyncio.CancelledError:
                        logger.debug('The coroutine callback has been cancelled during the processing of a ' +
                                     'websocket event. Non fatal error - continuing')

                except ws.ConnectionClosed:
                    logger.debug('Connection was closed')
                    await self.reconnect()
                except Exception as e:
                    logger.error(f'Encountered exception: {e}')
                    await self.reconnect()
            self.close_websocket()
        self.closed = True

    async def _send_msg(self, message: Dict, private_msg=False) -> None:
        """
        Sends a message on the websocket connection. Private function because it does
        not retry on failure

        :param message: The python dict containing the message info
        :param private_msg: Whether it is a public or private WS message
        :return: None
        """
        # set parameters of the message
        message['id'] = make_kucoin_unique_id_str()
        message['privateChannel'] = private_msg

        json_payload = json.dumps(message)

        await self.websocket.send(json_payload)

    async def send_ws_message(self, message: Dict, num_retries=5, private_msg=False) -> None:
        """
        Sends a message on the websocket. If the websocket is not connected it will
        wait 1 second and try again. Will continue trying until num_retries

        :param message: The message to send
        :param num_retries: The number of times to try again if the socket is not connected
        :param private_msg: Whether it is a public or private WS message
        :return: None
        """
        if not self.websocket:
            count = 0
            while not self.websocket and count < num_retries:
                await asyncio.sleep(1)
                await self._send_msg(message, private_msg=private_msg)
        else:
            await self._send_msg(message, private_msg=private_msg)

    async def send_ping_message(self) -> None:
        """
        Sends a ping message to the server on the websocket

        :return: None
        """
        message = {
            'type': 'ping',
            'id': make_kucoin_unique_id_str()
        }
        # print('sending ping')
        await self.send_ws_message(message, private_msg=False)

        self.last_ping_time = time.time()

    async def subscribe(self, topic: str, private_msg=False) -> None:
        """
        Subscribes to a websocket channel

        :param topic:
        :param private_msg:
        :return: None
        """
        message = {
            'type': 'subscribe',
            'topic': topic,
            'response': True
        }

        await self.send_ws_message(message, private_msg=private_msg)

    async def unsubscribe(self, topic: str, private_msg=False) -> None:
        """
        Unsubscribes from a websocket channel

        :param topic:
        :param private_msg:
        :return: None
        """
        message = {
            'type': 'unsubscribe',
            'topic': topic,
            'response': True
        }
        await self.send_ws_message(message, private_msg=private_msg)
