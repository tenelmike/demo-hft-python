"""
Contains some helper functions for making REST requests like setting headers and signing authentication for the API
"""
import base64
import hashlib
import hmac
import json
import os
from dotenv import load_dotenv

load_dotenv()  # Load the .env file into the environment
KUCOIN_API_SECRET = str(os.getenv('KUCOIN_API_SECRET'))
KUCOIN_API_KEY = str(os.getenv('KUCOIN_API_KEY'))
KUCOIN_API_PASSWORD = str(os.getenv('KUCOIN_API_PASSWORD'))


def encode_signature_string(signature_str: bytes):
    """
    Encodes the signature string into a base64 encoding using a sha256 HMAC

    :param signature_str: the string to encode
    :return: the base64 encoded string
    """
    message = hmac.new(KUCOIN_API_SECRET.encode('utf-8'), signature_str, hashlib.sha256)
    return base64.b64encode(message.digest())


def generate_get_rest_signature(nonce, path, params):
    """
    Generates API signature for HTTP header to authenticate GET REST calls

    :param nonce:
    :param path:
    :param params:
    :return:
    """
    if params is not None:
        endpoint = "{}?{}".format(path, '&'.join(["{}={}".format(key, params[key]) for key in params]))
    else:
        endpoint = path
    signature_str = ("{}{}{}{}".format(nonce, 'GET', endpoint, '')).encode('utf-8')
    return encode_signature_string(signature_str)


def generate_rest_signature(nonce, method, path, data):
    """
    Generates API signature for HTTP header to authenticate REST calls

    :param nonce:
    :param method:
    :param path:
    :param data:
    :return:
    """
    data_json = ''
    if data:
        # data_json = json.dumps(data, separators=(',', ':'), ensure_ascii=False)
        data_json = json.dumps(data, separators=(',', ':'))
    signature_str = ("{}{}{}{}".format(nonce, method.upper(), path, data_json)).encode('utf-8')
    # print(signature_str)
    return encode_signature_string(signature_str)


def build_http_headers():

    ua_str = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) ' + \
             'AppleWebKit/537.36 (KHTML, like Gecko) ' + \
             'Chrome/90.0.4430.212 Safari/537.36'
    headers = {
        'Accept': 'application/json',
        'User-Agent': ua_str,
        'Content-Type': 'application/json',
        'KC-API-KEY': KUCOIN_API_KEY,
        'KC-API-PASSPHRASE': base64.b64encode(hmac.new(
            KUCOIN_API_SECRET.encode('utf-8'), KUCOIN_API_PASSWORD.encode('utf-8'),
            hashlib.sha256).digest()).decode('utf-8'),
        'KC-API-KEY-VERSION': '2'
    }

    return headers
