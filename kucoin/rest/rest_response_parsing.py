"""
Contains all of the functions for parsing responses from the REST API.

These functions are separated from the request so that they can be awaited by the
calling coroutine functions at the appropriate time and do not block the requesting.

This allows for multiple concurrent REST requests from the same thread using the aiohttp library
"""
import logging

from decimal import Decimal
from typing import List, Union

from kucoin.kucoin_dataclasses import (LendingRate, LendingRates, MarginInfo, WebsocketInfo,
                                       SymbolInfo, TickerInfo, TickerHistoryEvent, AccountInfo)
from kucoin.utils import (get_kucoin_system_error_message, get_http_error_message)
from order_book import OrderBook

# Create a logger object
logger = logging.getLogger('kucoin.rest')

# Set the log level to DEBUG so that all messages are logged
logger.setLevel(logging.DEBUG)

# create a handler for debug messages
debug_handler = logging.StreamHandler()
debug_handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
debug_handler.setFormatter(formatter)
logger.addHandler(debug_handler)


async def await_response(response_future):
    """
    Takes a response future object and blocks the calling coroutine until the response has been received from
    the API and can be parsed.

    :param response_future: The future object to be awaited
    :return: The parsed JSON data dict
    """
    response_future = await response_future
    response_body = await response_future.json()
    http_status_code = response_future.status

    kucoin_system_code = int(response_body['code'])

    if http_status_code != 200 or kucoin_system_code != 200000:
        logger.error('Error sending Request: \n' +
                     f'{get_http_error_message(http_status_code)} ' +
                     f'{get_kucoin_system_error_message(kucoin_system_code)}')
        return None

    return response_body['data']


async def wait_and_parse_websocket_channel_info(response_future):
    """
    Takes a response future object from the get_websocket_channel_info REST API call and awaits for the
    response. The response is parsed and returned as a WebsocketInfo dataclass

    :param response_future: The response future to be awaited
    :return: The WebsocketInfo object containing the parsed response
    """
    data_dict = await await_response(response_future)

    instance_server = data_dict['instanceServers'][0]

    ws_connect_token = data_dict['token']
    endpoint = instance_server['endpoint']
    encrypt = bool(instance_server['encrypt'])
    protocol = instance_server['protocol']
    ping_interval = int(instance_server['pingInterval'])
    ping_timeout = int(instance_server['pingTimeout'])

    ws_info = WebsocketInfo(
        ws_connect_token=ws_connect_token,
        endpoint=endpoint,
        encrypt=encrypt,
        protocol=protocol,
        ping_interval=ping_interval,
        ping_timeout=ping_timeout
    )

    return ws_info


async def wait_and_parse_ticker_history(response_future):
    """
    Takes a response future object from the get_ticker_history REST API call and awaits for the
    response. The response is parsed and the ticker history is returned

    :param response_future: The response future to be awaited
    :return: The ticker history List containing the parsed response
    """
    data_list = await await_response(response_future)

    history_list = []
    for data_dict in data_list:
        sequence = int(data_dict['sequence'])
        price = Decimal(data_dict['price'])
        size = Decimal(data_dict['size'])
        side = str(data_dict['side'])
        timestamp = int(data_dict['time'])

        history = TickerHistoryEvent(
            sequence=sequence,
            price=price,
            size=size,
            side=side,
            time=timestamp  # seems to be in ns
        )
        history_list.append(history)

    return history_list


async def wait_and_parse_order_book_snapshot(
        response_future, current_price: Decimal, price_increment: Decimal) -> OrderBook:
    """
    Takes a response future object from the get_order_book_snapshot REST API call and awaits for the
    response. The response is parsed and the order book snapshot is returned

    :param response_future: The response future to be awaited
    :param current_price: The current midpoint price around which the order book is centered
    :param price_increment: The minimum price increment between levels of the order book
    :return: The OrderBook containing the parsed response
    """
    data_dict = await await_response(response_future)

    sequence = int(data_dict['sequence'])
    _timestamp = int(data_dict['time'])

    order_book = OrderBook(current_price, price_increment)
    order_book.snapshot_update(sequence, data_dict['bids'], data_dict['asks'])

    return order_book


async def wait_and_parse_cancel_order(response_future):
    """
    Takes a response future object from the cancel_order REST API call and awaits for the
    response. The response is parsed and the id of the cancelled order is returned

    :param response_future: The response future to be awaited
    :return: The id of the cancelled order in the parsed response
    """
    data_dict = await await_response(response_future)
    order_id = str(data_dict['cancelledOrderIds'][0])
    return order_id


async def wait_and_parse_cancel_all_orders(response_future):
    """
    Takes a response future object from the cancel_all_orders REST API call and awaits for the
    response. The response is parsed and the ids of the cancelled orders is returned

    :param response_future: The response future to be awaited
    :return: The List containing the ids of cancelled orders in the parsed response
    """
    data_dict = await await_response(response_future)
    return data_dict['cancelledOrderIds']


async def wait_and_parse_list_all_orders(response_future):
    """
    Takes a response future object from the list_all_orders REST API call and awaits for the
    response. The response is parsed and the List of the orders is returned containing all the info for each

    :param response_future: The response future to be awaited
    :return: The List containing the orders in the parsed response
    """
    data_dict = await await_response(response_future)

    _current_page = int(data_dict['currentPage'])
    _page_size = int(data_dict['pageSize'])
    _total_num = int(data_dict['totalNum'])
    _total_pages = int(data_dict['totalPage'])

    items = data_dict['items']

    orders = []
    for item in items:
        _order_id = str(item['id'])  # order id
        _symbol = str(item['symbol'])  # symbol
        _op_type = str(item['opType'])  # operation type: DEAL
        _order_type = str(item['type'])  # order type,e.g. limit,market,stop_limit.
        _side = str(item['side'])  # transaction direction,include buy and sell
        _price = float(item['price'])  # order price
        _quantity = float(item['size'])  # order quantity
        _funds = float(item['funds'])  # order funds
        _deal_funds = float(item['dealFunds'])  # deal funds
        _deal_quantity = float(item['dealSize'])  # deal quantity
        _fee = float(item['fee'])  # fee amount
        _fee_currency = str(item['feeCurrency'])  # charge fee currency
        _self_trade_prevention = str(item['stp'])  # self trade prevention,include CN,CO,DC,CB
        _stop_type = str(item['stop'])  # stop type
        _stop_triggered = bool(item['stopTriggered'])  # is the stop triggered
        _stop_price = float(item['stopPrice'])  # the price of the stop
        _time_in_force = str(item['timeInForce'])  # time InForce,include GTC,GTT,IOC,FOK
        _post_only = bool(item['postOnly'])  # is post only order
        _hidden = bool(item['hidden'])  # is hidden order
        _iceberg = bool(item['iceberg'])  # is iceberg order
        _visible_size = float(item['visibleSize'])  # display quantity for iceberg order
        _cancel_after = int(item['cancelAfter'])  # cancel orders time, requires timeInForce to be GTT
        _channel = str(item['channel'])  # order source
        _client_oid = str(item['clientOid'])  # client order id
        _remark = str(item['remark'])  # user remark
        _tags = str(item['tags'])  # tag order source
        _is_active = bool(item['isActive'])  # status before unfilled or uncancelled
        _cancel_exist = bool(item['cancelExist'])  # order cancellation transaction record
        _created_at = int(item['createdAt'])  # create time
        _trade_type = str(item['tradeType'])  # "TRADE"

        orders.append([_order_id, _symbol, _order_type, _side, _price, _quantity, _fee, _client_oid, _created_at])
    return orders


async def wait_and_parse_create_limit_order(response_future):
    """
    Takes a response future object from the create_limit_order REST API call and awaits for the
    response. The response is parsed and the id of the limit order is returned

    :param response_future: The response future to be awaited
    :return: The id of the limit order in the parsed response
    """
    data_dict = await await_response(response_future)

    # print(data_dict)
    return data_dict['orderId']


async def wait_and_parse_get_all_symbol_info(response_future) -> List[SymbolInfo]:
    """
    Takes a response future object from the get_all_symbol_info REST API call and awaits for the
    response. The response is parsed and the information about each trading symbol on the exchange is returned
    in a List

    :param response_future: The response future to be awaited
    :return: The List of trading symbol information in the parsed response
    """
    data_dict = await await_response(response_future)

    symbol_infos = []
    for s in data_dict:

        mf = s["minFunds"]
        if mf is None:
            mf = Decimal(0.000000001)

        symbol = SymbolInfo(
            symbol=s["symbol"],
            name=s["name"],
            base_currency=s["baseCurrency"],
            quote_currency=s["quoteCurrency"],
            fee_currency=s["feeCurrency"],
            market=s["market"],
            base_min_size=Decimal(s["baseMinSize"]),
            quote_min_size=Decimal(s["quoteMinSize"]),
            base_max_size=Decimal(s["baseMaxSize"]),
            quote_max_size=Decimal(s["quoteMaxSize"]),
            base_increment=Decimal(s["baseIncrement"]),
            quote_increment=Decimal(s["quoteIncrement"]),
            price_increment=Decimal(s["priceIncrement"]),
            price_limit_rate=Decimal(s["priceLimitRate"]),
            min_funds=Decimal(mf),
            is_margin_enabled=s["isMarginEnabled"],
            enable_trading=s["enableTrading"]
        )
        symbol_infos.append(symbol)
    return symbol_infos


async def wait_and_parse_get_symbol_info(response_future, symbol: str) -> Union[None, SymbolInfo]:
    """
    Takes a response future object from the get_all_symbol_info REST API call and awaits for the
    response. The response is parsed and the information about a specific trading symbol on the exchange is returned.
    If the specific symbol is not present in the List, the call returns None.

    :param response_future: The response future to be awaited
    :param symbol: The trading symbol info of interest
    :return: The trading symbol information in the parsed response or None
    """
    symbol_infos = await wait_and_parse_get_all_symbol_info(response_future)

    for s in symbol_infos:
        if s.symbol == symbol:
            return s

    return None


async def wait_and_parse_get_ticker(response_future) -> TickerInfo:
    """
    Takes a response future object from the get_ticker REST API call and awaits for the
    response. The response is parsed and the information about a specific ticker's best bid and ask on the
    exchange is returned as a TickerInfo object

    :param response_future: The response future to be awaited
    :return: The best bid and ask info in the parsed response as a TickerInfo object
    """
    data_dict = await await_response(response_future)

    ticker = TickerInfo(
        sequence=int(data_dict['sequence']),
        best_ask=Decimal(data_dict['bestAsk']),
        size=Decimal(data_dict['size']),
        price=Decimal(data_dict['price']),
        best_bid=Decimal(data_dict['bestBid']),
        best_ask_size=Decimal(data_dict['bestAskSize']),
        best_bid_size=Decimal(data_dict['bestBidSize']),
        time=(data_dict['time'])
    )

    return ticker


async def wait_and_parse_get_timestamp(response_future):
    """
    Takes a response future object from the get_timestamp REST API call and awaits for the
    response. The response is parsed and the server timestamp is returned

    :param response_future: The response future to be awaited
    :return: The server timestamp
    """
    data_dict = await await_response(response_future)

    return data_dict


async def wait_and_parse_get_accounts(response_future, acct_type='trade', currencies=()):
    """
    Takes a response future object from the get_accounts REST API call and awaits for the
    response. The response is parsed and the AccountInfo is returned for each currency

    :param response_future: The response future to be awaited
    :param acct_type: The account type like trade, isolated, etc.
    :param currencies: The currency accounts we are looking for
    :return: The dict containing account['currency'] = AccountInfo
    """
    data_dict = await await_response(response_future)

    accounts = {}
    for acct in data_dict:
        logger.debug(f'Account: {acct}')
        if acct['type'] == acct_type:

            for cur in currencies:
                if cur == acct['currency'] and cur not in accounts:
                    accounts[cur] = AccountInfo(
                        id=str(acct['id']),
                        currency=str(acct['currency']),
                        account_type=str(acct['type']),
                        total_balance=Decimal(acct['balance']),
                        available_balance=Decimal(acct['available']),
                        holds_balance=Decimal(acct['holds'])
                    )
                elif cur == acct['currency'] and cur in accounts:
                    if float(acct['balance']) >= accounts[cur].total_balance:
                        accounts[cur] = AccountInfo(
                            id=str(acct['id']),
                            currency=str(acct['currency']),
                            account_type=str(acct['type']),
                            total_balance=Decimal(acct['balance']),
                            available_balance=Decimal(acct['available']),
                            holds_balance=Decimal(acct['holds'])
                        )

    return accounts


async def wait_and_parse_get_isolated_margin_account_info(response_future):
    """
    Takes a response future object from the get_isolated_margin_account_info REST API call and awaits for the
    response. The response is parsed and the MarginInfo is returned

    Example response:
    "symbol": "MANA-USDT",
    "status": "CLEAR",
    "debtRatio": "0",
    "baseAsset": {
        "currency": "MANA",
        "totalBalance": "0",
        "holdBalance": "0",
        "availableBalance": "0",
        "liability": "0",
        "interest": "0",
        "borrowableAmount": "0"
    },
    "quoteAsset": {
        "currency": "USDT",
        "totalBalance": "0",
        "holdBalance": "0",
        "availableBalance": "0",
        "liability": "0",
        "interest": "0",
        "borrowableAmount": "0"
    }
    """
    data_dict = await await_response(response_future)

    status = str(data_dict['status'])
    debt_ratio = Decimal(data_dict['debtRatio'])
    borrowed_liability = Decimal(data_dict['baseAsset']['liability'])
    accrued_interest = Decimal(data_dict['baseAsset']['interest'])
    borrowable_amount = Decimal(data_dict['baseAsset']['borrowableAmount'])

    return MarginInfo(
        status=status,
        debt_ratio=debt_ratio,
        borrowed_liability=borrowed_liability,
        accrued_interest=accrued_interest,
        borrowable_amount=borrowable_amount
    )


async def wait_and_parse_get_lending_market_rate(response_future):
    """
    Takes a response future object from the get_lending_market_rate REST API call and awaits for the
    response. The response is parsed and the LendingRates containing the loan prices is returned

    :param response_future: The response future to be awaited
    :return: The LendingRates
    """
    data_dict = await await_response(response_future)

    rate_list = []
    for rate_dict in data_dict:
        rate_list.append(LendingRate(
            daily_interest_rate=Decimal(rate_dict['dailyIntRate']),
            term_days=int(rate_dict['term']),
            amount=Decimal(rate_dict['size'])
        ))
    return LendingRates(rate_list=rate_list)


async def wait_and_parse_create_margin_limit_order(response_future):
    """
    Takes a response future object from the create_margin_limit_order REST API call and awaits for the
    response. The response is parsed and the id of the limit order is returned

    Example response:
    {
        "orderId": "5bd6e9286d99522a52e458de",
        "borrowSize":10.2,
        "loanApplyId":"600656d9a33ac90009de4f6f"
    }

    :param response_future: The response future to be awaited
    :return: The id of the limit order in the parsed response, the size of any borrowed amount, the loan id
    """
    data_dict = await await_response(response_future)

    if data_dict is None:
        return None

    order_id = str(data_dict['orderId'])

    if data_dict['borrowSize'] is not None:
        borrow_size = Decimal(data_dict['borrowSize'])
    else:
        borrow_size = None

    if data_dict['loanApplyId'] is not None:
        loan_apply_id = str(data_dict['loanApplyId'])
    else:
        loan_apply_id = None

    return order_id, borrow_size, loan_apply_id
