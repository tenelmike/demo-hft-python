"""
Contains the implementation of the Kucoin REST API using the aiohttp library.

The KucoinRestClient class creates a connection and is responsible for making requests to the
API. After each request the class returns a future object so that the calling coroutine can burst many
concurrent REST requests from the same thread without blocking.

The responses are parsed using separate functions and can be called at the appropriate time to not block requesting
"""
import copy
import json
import logging
import uuid
import time
import asyncio
import aiohttp
from kucoin.rest.rest_request_helpers import (generate_get_rest_signature, generate_rest_signature, build_http_headers)


# Create a logger object
logger = logging.getLogger('kucoin.rest')

# Set the log level to DEBUG so that all messages are logged
logger.setLevel(logging.DEBUG)


class KucoinRestClient:

    REST_API_URL = 'https://openapi-v2.kucoin.com'
    SANDBOX_API_URL = 'https://openapi-sandbox.kucoin.com'

    def __init__(self, loop, sandbox=False):
        """
        Creates a REST client for Kucoin

        :param loop: The asyncio event loop
        :param sandbox: Whether to use the real or sandbox API
        """
        if sandbox:
            self.API_URL = self.SANDBOX_API_URL
        else:
            self.API_URL = self.REST_API_URL

        self.loop = loop

        self.headers_dict = build_http_headers()
        self.session = None
        self.shutdown = False

    def terminate(self):
        # _loop = asyncio.get_event_loop()
        self.loop.create_task(self.close())

    async def close(self):
        logger.info('Closing REST Session')
        await self.session.close()
        self.shutdown = True
        logger.info('REST Session Closed')

    async def get_request(self, path_ending: str, params: dict = None):
        """
        Performs an async GET request using the aiohttp library to the KuCoin REST API.

        :param path_ending: The end of the url path like /api/v1/timestamp
        :param params: The url parameters to include in the end of the URL as key-value pairs
        :return: The response future object that can be awaited by the calling coroutine
        """
        if self.session is None:
            self.session = aiohttp.ClientSession()

        url = self.API_URL + path_ending

        # create the request headers
        request_headers = copy.deepcopy(self.headers_dict)
        request_headers['timeout'] = str(10)

        # sign the request
        # from the API docs
        now = int(time.time() * 1000)
        request_headers['KC-API-TIMESTAMP'] = str(now)
        request_headers['KC-API-SIGN'] = generate_get_rest_signature(now, path_ending, params).decode('utf-8')

        response_future = asyncio.ensure_future(
            self.session.get(url, headers=request_headers, params=params,
                             timeout=aiohttp.ClientTimeout(total=None, sock_connect=10, sock_read=10)), loop=self.loop)

        return response_future

    async def post_request(self, path_ending: str, body_data: dict):
        """
        Performs an async POST request using the aiohttp library to the KuCoin REST API.

        :param path_ending: The end of the url path like /api/v1/timestamp
        :param body_data: The JSON to include in the body of the REST request
        :return: The response future object that can be awaited by the calling coroutine
        """
        if self.session is None:
            self.session = aiohttp.ClientSession()

        url = self.API_URL + path_ending

        # create the request headers
        request_headers = copy.deepcopy(self.headers_dict)
        request_headers['timeout'] = str(10)

        # sign the request
        # from the API docs
        now = int(time.time() * 1000)
        request_headers['KC-API-TIMESTAMP'] = str(now)
        request_headers['KC-API-SIGN'] = generate_rest_signature(
            str(now), 'POST', path_ending, body_data).decode('utf-8')

        if body_data == {}:
            body_data = None
        else:
            body_data = json.dumps(body_data, separators=(',', ':'))

        response_future = asyncio.ensure_future(
            self.session.post(url, headers=request_headers, data=body_data,
                              timeout=aiohttp.ClientTimeout(total=None, sock_connect=10, sock_read=10)), loop=self.loop)

        return response_future

    async def delete_request(self, path_ending: str, body_data: dict):
        """
        Performs an async DELETE request using the aiohttp library to the KuCoin REST API.

        :param path_ending: The end of the url path like /api/v1/timestamp
        :param body_data: The JSON to include in the body of the REST request
        :return: The response future object that can be awaited by the calling coroutine
        """
        if self.session is None:
            self.session = aiohttp.ClientSession()

        url = self.API_URL + path_ending

        # create the request headers
        request_headers = copy.deepcopy(self.headers_dict)
        request_headers['timeout'] = str(10)

        # sign the request
        # from the API docs
        now = int(time.time() * 1000)
        request_headers['KC-API-TIMESTAMP'] = str(now)
        request_headers['KC-API-SIGN'] = generate_rest_signature(
            str(now), 'DELETE', path_ending, body_data).decode('utf-8')

        if body_data == {}:
            body_data = None
        else:
            body_data = json.dumps(body_data, separators=(',', ':'))

        response_future = asyncio.ensure_future(
            self.session.delete(url, headers=request_headers, data=body_data,
                                timeout=aiohttp.ClientTimeout(
                                    total=None, sock_connect=10, sock_read=10)), loop=self.loop)

        return response_future

    async def put_request(self, path_ending: str, body_data: dict):
        """
        Performs an async PUT request using the aiohttp library to the KuCoin REST API.

        :param path_ending: The end of the url path like /api/v1/timestamp
        :param body_data: The JSON to include in the body of the REST request
        :return: The response future object that can be awaited by the calling coroutine
        """
        if self.session is None:
            self.session = aiohttp.ClientSession()

        url = self.API_URL + path_ending

        # create the request headers
        request_headers = copy.deepcopy(self.headers_dict)
        request_headers['timeout'] = str(10)

        # sign the request
        # from the API docs
        now = int(time.time() * 1000)
        request_headers['KC-API-TIMESTAMP'] = str(now)
        request_headers['KC-API-SIGN'] = generate_rest_signature(
            str(now), 'PUT', path_ending, body_data).decode('utf-8')

        if body_data == {}:
            body_data = None
        else:
            body_data = json.dumps(body_data, separators=(',', ':'))

        response_future = asyncio.ensure_future(
            self.session.put(url, headers=request_headers, data=body_data,
                             timeout=aiohttp.ClientTimeout(
                                 total=None, sock_connect=10, sock_read=10)), loop=self.loop)
        return response_future

    async def get_timestamp(self):
        """
        Requests to get the server timestamp from the REST API and returns a response
        future object so that the calling coroutine can await the result when it is ready

        :return: The response future to be awaited
        """
        return await self.get_request('/api/v1/timestamp')

    async def get_status(self):
        return await self.get_request('/api/v1/status')

    async def get_currencies(self):
        return await self.get_request('/api/v1/currencies')

    async def get_currency(self, symbol):
        return await self.get_request(f'/api/v2/currencies/{symbol}')

    async def get_accounts(self):
        return await self.get_request('/api/v1/accounts')

    async def get_account(self, account_id):
        return await self.get_request(f'/api/v1/accounts/{account_id}')

    async def get_ticker(self, symbol: str):
        """
        Requests to get information about the best bid and ask from the REST API and returns a response
        future object so that the calling coroutine can await the result when it is ready

        :param symbol: The trading symbol to get the TickerInfo on, e.g. BTC-USDT
        :return: The response future to be awaited
        """
        return await self.get_request(f'/api/v1/market/orderbook/level1?symbol={symbol}')

    async def get_all_symbol_info(self):
        """
        Requests to get all trading symbol info from the REST API and returns a response future object so that the
        calling coroutine can await the result when it is ready

        :return: The response future to be awaited
        """
        return await self.get_request(f'/api/v2/symbols')

    async def create_limit_order(self, symbol: str, side: str, price, size, tif, cancel_after):
        """
        Requests to create a limit order from the REST API and returns a response future object so that the
        calling coroutine can await the result when it is ready

        :param symbol: The trading symbol we are creating a limit order for
        :param side: The side buy or sell for the order
        :param price: The price of the limit order in the Order Book
        :param size: The size of the order in amount to trade
        :param tif: The Time in Force. Can be Good Til Canceled, Good Til Time, etc.
        :param cancel_after: The time to cancel the order if specified GTT for tif
        :return: The response future to be awaited
        """
        body_data = {
            'clientOid': str(uuid.uuid4()),
            'symbol': symbol,
            'side': side,
            'type': 'limit',
            'price': price,
            'size': size,
            'timeInForce': tif,
        }

        if tif == 'GTT':
            body_data['cancelAfter'] = cancel_after
        # print(body_data)
        return await self.post_request('/api/v1/orders', body_data)

    async def create_margin_limit_order(self, symbol: str, side: str, price, size, tif, cancel_after):
        """
        Requests to create a limit order in the margin account from the REST API and returns a response future
        object so that the calling coroutine can await the result when it is ready

        :param symbol: The trading symbol we are creating a limit order for
        :param side: The side buy or sell for the order
        :param price: The price of the limit order in the Order Book
        :param size: The size of the order in amount to trade
        :param tif: The Time in Force. Can be Good Til Canceled, Good Til Time, etc.
        :param cancel_after: The time to cancel the order if specified GTT for tif
        :return: The response future to be awaited
        """
        body_data = {
            'clientOid': str(uuid.uuid4()),
            'symbol': symbol,
            'side': side,
            'type': 'limit',
            'price': price,
            'size': size,
            'timeInForce': tif,
            'marginModel': 'isolated',
            'autoBorrow': False
        }

        if tif == 'GTT':
            body_data['cancelAfter'] = cancel_after
        # print(body_data)
        return await self.post_request('/api/v1/margin/order', body_data)

    async def list_all_orders(self):
        """
        Requests the list of all active orders from the REST API and returns a response future object so that the
        calling coroutine can await the result when it is ready

        :return: The response future to be awaited
        """
        return await self.get_request('/api/v1/orders?status=active')

    async def cancel_order(self, order_id):
        """
        Requests to cancel a specific order from the REST API and returns a response future object so that the
        calling coroutine can await the result when it is ready

        :param order_id: The id of the order to request to be cancelled
        :return: The response future to be awaited
        """
        return await self.delete_request(f'/api/v1/orders/{order_id}', {})

    async def cancel_all_orders(self):
        """
        Requests to cancel all orders from the REST API and returns a response future object so that the
        calling coroutine can await the result when it is ready

        :return: The response future to be awaited
        """
        return await self.delete_request('/api/v1/orders', {})

    async def get_orders(self):
        return await self.get_request('/api/v1/orders')

    async def get_order(self, order_id):
        return await self.get_request(f'/api/v1/orders/{order_id}')

    async def get_order_history(self):
        return await self.get_request('/api/v1/limit/orders')

    async def get_order_book_snapshot(self, symbol: str, depth: int = 100):
        """
        Requests a snapshot of the Order Book at a certain moment in time up to a certain depth from the REST API and
        returns a response future object so that the calling coroutine can await the result when it is ready

        :param symbol: The trading symbol of the order book to be requested like BTC-USDT
        :param depth: The depth of the order book to request
        :return: The response future to be awaited
        """
        return await self.get_request(f'/api/v1/market/orderbook/level2_{depth}?symbol={symbol}')

    async def get_ticker_history(self, symbol: str):
        """
        Requests the ticker history of price, size, side info of recent trades for a given trading symbol from
        the REST API and returns a response future object so that the calling coroutine can await the result
        when it is ready

        :param symbol: The trading symbol like BTC-USDT
        :return: The response future to be awaited
        """
        return await self.get_request(f'/api/v1/market/histories?symbol={symbol}')

    async def request_websocket_channel_info(self):
        """
        Requests the websocket channel info from the REST API and returns a response future object so that the
        calling coroutine can await the result when it is ready

        :return: The response future to be awaited
        """
        return await self.post_request('/api/v1/bullet-private', {})

    async def get_isolated_margin_account_info(self, symbol: str):
        """
        Requests the information of the isolated margin account from the REST API and returns a response future
        object so that the calling coroutine can await the result when it is ready

        :param symbol: The trading symbol ticker like BTC-USDT
        :return: The response future to be awaited
        """
        return await self.get_request(f'/api/v1/isolated/account/{symbol}')

    async def get_lending_market_rate(self, asset: str, term_days: int):
        """
        Requests the interest rate from the lending market for the asset and loan term duration in days from
        the REST API and returns a response future object so that the calling coroutine can await the result when
        it is ready

        :param asset: The asset like BTC to get the interest rate for
        :param term_days: The number of days of the loan, can be 7, 14, 28
        :return: The response future to be awaited
        """
        return await self.get_request(f'/api/v1/margin/market?currency={asset}&term={term_days}')
