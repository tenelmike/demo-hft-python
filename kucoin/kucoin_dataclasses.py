
from decimal import Decimal
from dataclasses import dataclass
from typing import List


@dataclass
class AccountInfo:
    """
    Dataclass for storing info about exchange accounts
    """
    # account id string
    id: str

    # account currency symbol
    currency: str

    # account type string
    account_type: str

    # account total balance (available + holds)
    total_balance: Decimal

    # available balance
    available_balance: Decimal

    # hold balance (pledged to orders not yet executed or to transfers, etc.)
    holds_balance: Decimal

    def __repr__(self):
        return f'AccountInfo(id={self.id}, symbol={self.currency}, type={self.account_type}, \n' \
               f'total_balance={self.total_balance}, available_balance={self.available_balance}, \n' \
               f'holds_balance={self.holds_balance})\n'


@dataclass
class TickerInfo:
    """
    Dataclass for storing info about ticker
    """
    # sequence number
    sequence: int

    # best ask price
    best_ask: Decimal

    # last traded size
    size: Decimal

    # last traded price
    price: Decimal

    # best bid price
    best_bid: Decimal

    # best ask size
    best_ask_size: Decimal

    # best bid size
    best_bid_size: Decimal

    # timestamp
    time: int


@dataclass
class SymbolInfo:
    """
    Dataclass for storing info about trading symbols
    """
    # unique code of a symbol, will not change after renaming
    symbol: str

    # Name of trading pairs, will change after renaming
    name: str

    # Base currency, e.g. BTC
    base_currency: str

    # Quote currency, e.g. USDT.
    quote_currency: str

    # the currency of charged fees
    fee_currency: str

    # the trading market
    market: str

    # the minimum order quantity requried to place an order
    base_min_size: Decimal

    # the minimum order funds required to place a market order
    quote_min_size: Decimal

    # the maximum order size required to place an order
    base_max_size: Decimal

    # the maximum order funds required to place a market order
    quote_max_size: Decimal

    # the increment of the order size. The value shall
    # be a positive multiple of the baseIncrement
    base_increment: Decimal

    # the increment of the funds required to place a
    # market order. The value shall be a positive
    # multiple of the quoteIncrement
    quote_increment: Decimal

    # the increment of the price required to place a limit
    # order. The value shall be a positive multiple of the
    # priceIncrement
    price_increment: Decimal

    # threshold for price portection
    price_limit_rate: Decimal

    # the minimum spot and margin trading amounts
    min_funds: Decimal

    # available for margin or not
    is_margin_enabled: bool

    # available for transaction or not
    enable_trading: bool

    def __repr__(self):
        return f"SymbolInfo(symbol={self.symbol}, name={self.name}, " \
               f"base_currency={self.base_currency}, " \
               f"quote_currency={self.quote_currency}, " \
               f"fee_currency={self.fee_currency}, market={self.market}, " \
               f"base_min_size={self.base_min_size}, " \
               f"quote_min_size={self.quote_min_size}, " \
               f"base_max_size={self.base_max_size}, " \
               f"quote_max_size={self.quote_max_size}, " \
               f"base_increment={self.base_increment}, " \
               f"quote_increment={self.quote_increment}, " \
               f"price_increment={self.price_increment}, " \
               f"price_limit_rate={self.price_limit_rate}, " \
               f"min_funds={self.min_funds}, " \
               f"is_margin_enabled={self.is_margin_enabled}, " \
               f"enable_trading={self.enable_trading})"


@dataclass
class WebsocketInfo:
    """
    Dataclass for storing info about the websocket configuration
    """
    # token to connect the websocket session
    ws_connect_token: str

    # the URL to connect to
    endpoint: str

    # whether the websocket needs to use SSL
    encrypt: bool

    # the protocol to use - should always be 'websocket'
    protocol: str

    # the interval the client should send a ping to keep the connection open
    ping_interval: int

    # the extreme timeout value where the server will close the connection if no ping is received
    ping_timeout: int

    def __repr__(self):
        ws_str = f"WebSocket connection token: {self.ws_connect_token}\n"
        ws_str += f"Endpoint URL: {self.endpoint}\n"
        ws_str += f"Encryption required: {self.encrypt}\n"
        ws_str += f"Protocol: {self.protocol}\n"
        ws_str += f"Ping interval: {self.ping_interval} ms\n"
        ws_str += f"Ping timeout: {self.ping_timeout} ms\n"
        return ws_str


@dataclass
class TickerHistoryEvent:
    """
    Dataclass for storing trade history entries
    """
    # The sequence number, ex. 1545896668578
    sequence: int

    # The price, ex. 24567.8
    price: Decimal

    # The amount traded, ex. 0.066
    size: Decimal

    # buy or sell, ex. 'buy'
    side: str

    # The server time ex. 1545904581619888405
    time: int

    def __repr__(self):
        th_str = f"Ticker History:\nSequence: {self.sequence}\n"
        th_str += f"Price: {self.price}\n"
        th_str += f"Size: {self.size}\n"
        th_str += f"Side: {self.side}\n"
        th_str += f"Time: {self.time} ns\n"
        return th_str


@dataclass
class LendingRate:

    # Daily interest rate. e.g. 0.002 is 0.2%
    daily_interest_rate: Decimal

    # Term of the loan (Unit: Day)
    term_days: int

    # Total size available at this interest rate and term in the market
    amount: Decimal


@dataclass
class LendingRates:

    # the list of lending rates in the market
    rate_list: List[LendingRate]


@dataclass
class MarginInfo:

    # The position status: Existing liabilities-DEBT,
    # No liabilities-CLEAR,
    # Bankrupcy (after position enters a negative balance)-BANKRUPTCY,
    # Existing borrowings-IN_BORROW,
    # Existing repayments-IN_REPAY,
    # Under liquidation-IN_LIQUIDATION,
    # Under auto-renewal-IN_AUTO_RENEW (permissions per state)
    status: str

    # Debt ratio measuring how close account is to liquidation
    debt_ratio: Decimal

    # principal amount of an asset borrowed
    borrowed_liability: Decimal

    # the amount of accrued interest of the asset that was borrowed (repay amt borrowed + interest)
    accrued_interest: Decimal

    # The borrowable amount remaining
    borrowable_amount: Decimal

    def __repr__(self):
        return f'MarginInfo(status={self.status}, debt_ratio={self.debt_ratio}, \n' \
               f'borrowed_liability={self.borrowed_liability}, accrued_interest={self.accrued_interest}, \n' \
               f'borrowable_amount={self.borrowable_amount})\n'

