
from numba import jit
import numpy as np
from decimal import Decimal

from typing import List

from asyncio import Event


@jit(nopython=True)
def find_first_value(value_to_find: float, np_arr: np.ndarray):
    """
    numba function to find the first index of a value in a numpy array really fast

    :param value_to_find:
    :param np_arr:
    :return:
    """
    for i in range(len(np_arr)):
        if value_to_find == np_arr[i]:
            return i
    return -1


class BookSide:
    """
    Implements one side of the OrderBook, either bid or askW
    """

    def __init__(self, initial_price: Decimal, price_increment: Decimal, asks=True):
        """
        Initializes the book side

        :param initial_price: The price around which the order book is centered
        :param price_increment: The increment between ticks of each price level in the order book
        :param asks: Whether the side is for asks (True) or bids (False)
        """
        # the initial price to create the order book levels around
        self.initial_price = initial_price

        # the minimum price increment that divides the order book into discrete levels
        self.price_increment = price_increment

        # whether the book side is the bids or the asks (affects indexing)
        self.is_ask_book = asks

        # the total number of levels in the order book
        self.num_price_levels = 2 * (self.initial_price / self.price_increment)

        # the price levels data structure that stores the tradable amount at each price
        self.amounts = np.asarray([Decimal('0') for _ in range(int(self.num_price_levels))], dtype=np.float64)
        self.prices = np.asarray([self.get_price_from_index(i)
                                  for i in range(int(self.num_price_levels))], dtype=np.float64)

    def update_amount_at_price_level(self, price: Decimal, amount: Decimal):
        """
        Updates the quantity available for trade at the given price level

        :param price: The price level to update
        :param amount: The amount available to trade at the price level
        """
        index = self.get_index_from_price(price)
        self.amounts[index] = amount

    def get_amount_at_price_level(self, price: Decimal) -> Decimal:
        """
        Gets the amount available for trade at the specified price level

        :param price: The price level
        :return: The amount available for trade
        """
        index = self.get_index_from_price(price)
        return self.amounts[index]

    def get_index_from_price(self, price: Decimal) -> int:
        """
        Gets the index in the List from the specified price

        :param price: The price level to get the index
        :return: The index
        """
        return find_first_value(float(price), self.prices)

    def get_price_from_index(self, index: int) -> Decimal:
        """
        Gets the price level from a specified index

        :param index: The index in the List for the price level
        :return: The price level
        """
        if self.is_ask_book:
            return index * self.price_increment
        else:
            return (self.num_price_levels - index - 1) * self.price_increment

    def get_best_index(self):
        """
        Gets the best index in this side of the order book (the best bid or ask)

        :return: The index
        """
        if self.is_ask_book:
            return np.where(self.amounts != 0)[0][0]
        else:
            return np.where(self.amounts != 0)[0][0]


class OrderBook:
    """
    Implements the Order Book data structure
    """

    def __init__(self, initial_price: Decimal, price_increment: Decimal):
        """
        Initializes the bid and ask book around the specified price and price increments between levels of the order
        book

        :param initial_price: The price around which the order book is centered
        :param price_increment: The minimum price increment between price levels of the order book
        """
        self.bids = BookSide(initial_price, price_increment, asks=False)
        self.asks = BookSide(initial_price, price_increment, asks=True)

        self.sequence_number = 0

        self.update_flag = Event()

    def snapshot_update(self, sequence_num: int, bids: List[List[str]], asks: List[List[str]]):
        """
        Updates the order book with a snapshot of bids and ask prices

        :param sequence_num: The message number for the price snapshot
        :param bids: The bid snapshot
        :param asks: The ask snapshot
        """
        self.sequence_number = sequence_num

        for price, quantity in bids:
            self.bids.update_amount_at_price_level(Decimal(price), Decimal(quantity))

        for price, quantity in asks:
            self.asks.update_amount_at_price_level(Decimal(price), Decimal(quantity))

    def get_bbo(self):

        bid_index = self.bids.get_best_index()
        ask_index = self.asks.get_best_index()

        bid_price = self.bids.prices[bid_index]
        ask_price = self.asks.prices[ask_index]

        return bid_price, ask_price

    def get_midpoint_price(self):

        bid_price, ask_price = self.get_bbo()

        return (bid_price + ask_price) / 2.0
