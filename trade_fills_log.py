
import datetime


FILL_COLS = [
    'Date', 'Time', 'Side', 'Price (USD)', 'Quantity', 'Trade ID', 'Bid Order ID', 'Ask Order ID'
]


class TradeFills:
    """
    Logging class to manage order fills and keep a log of the trades for display
    """

    def __init__(self, max_fills=1000):
        """
        Constructs the class and initializes values to zero
        """
        self.fills = [{
            FILL_COLS[0]: '',
            FILL_COLS[1]: '',
            FILL_COLS[2]: '',
            FILL_COLS[3]: '',
            FILL_COLS[4]: '',
            FILL_COLS[5]: '',
            FILL_COLS[6]: '',
            FILL_COLS[7]: ''
        } for _ in range(max_fills)]
        self.fill_id_num = 0
        self.max_fills = max_fills
        self.ready = False

    def create_match_fill(self, timestamp_ns: int, side: str, price: float, quantity: float,
                          trade_id: str, maker_id: str, taker_id: str):
        """
        Creates a filled order when there is a match and adds it to the log of fills

        :param timestamp_ns: The timestamp of the server in nanoseconds when the match was filled
        :param side: The side of the trade, buy or sell
        :param price: The price the match happened
        :param quantity: The quantity matched
        :param trade_id: The id string of the trade
        :param maker_id: The id string of the provider of liquidity
        :param taker_id: The id string of the taker of liquidity
        """
        dt = datetime.datetime.fromtimestamp(timestamp_ns / 1000000000.0)
        date_str = dt.strftime('%d-%m-%Y')
        time_str = dt.strftime('%H:%M:%S::%f')

        if side == 'buy':
            bid_order_id = taker_id
            ask_order_id = maker_id
        else:
            bid_order_id = maker_id
            ask_order_id = taker_id

        self.fills[self.fill_id_num] = {
            FILL_COLS[0]: date_str,
            FILL_COLS[1]: time_str,
            FILL_COLS[2]: side,
            FILL_COLS[3]: price,
            FILL_COLS[4]: quantity,
            FILL_COLS[5]: trade_id,
            FILL_COLS[6]: bid_order_id,
            FILL_COLS[7]: ask_order_id,
            'timestamp_ns': timestamp_ns
        }

        self.fill_id_num += 1

        if self.fill_id_num == self.max_fills:
            self.ready = True
        self.fill_id_num = self.fill_id_num % self.max_fills
